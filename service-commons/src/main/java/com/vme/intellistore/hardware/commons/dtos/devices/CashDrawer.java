package com.vme.intellistore.hardware.commons.dtos.devices;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

/**
 * Created by Mirko on 25-Jan-17.
 */
@Getter @Setter @NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CashDrawer extends Hardware{

    //<editor-fold desc="Properties">

    @JsonProperty("Status")
    private CashDrawerStatus status;

    //</editor-fold>

    //<editor-fold desc="Constructors">

    public CashDrawer(HardwareActionType actionType) {
        super(actionType,HardwareDeviceType.CashDrawer);
        this.type = "Vme.Intellistore.Hardware.EndPoint.DataTransferObjects.PosObjects.CashDrawer, Vme.Intellistore.Hardware.EndPoint.DataTransferObjects";
    }

    public CashDrawer(String actionException, Boolean actionResult, HardwareActionType actionType) {
        super(actionException, actionResult, actionType,HardwareDeviceType.CashDrawer);
        this.type = "Vme.Intellistore.Hardware.EndPoint.DataTransferObjects.PosObjects.CashDrawer, Vme.Intellistore.Hardware.EndPoint.DataTransferObjects";
    }

    @Override
    public String toString() {
        return super.toString() + "CashDrawer{" +
                "status=" + status +
                '}';
    }

    //</editor-fold>
}
