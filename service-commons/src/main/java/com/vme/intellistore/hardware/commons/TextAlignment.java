package com.vme.intellistore.hardware.commons;

/**
 * Created by Mirko on 25-Jan-17.
 */
public enum TextAlignment {
    Center,
    Right,
    Left
}
