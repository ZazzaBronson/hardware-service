package com.vme.intellistore.hardware.commons.dtos.devices.scale;

/**
 * Created by Zazza on 07/06/2017.
 */
public enum ScaleFunctionType {
    ReadWeight,
    ResetWeight,
    DisplayText//,
    //SetAsync
}
