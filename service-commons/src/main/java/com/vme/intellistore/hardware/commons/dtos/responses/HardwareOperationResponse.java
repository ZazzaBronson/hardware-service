package com.vme.intellistore.hardware.commons.dtos.responses;

import com.vme.intellistore.hardware.commons.dtos.devices.Hardware;

import java.util.UUID;

/**
 * Created by Zazza on 02/05/2017.
 */
public class HardwareOperationResponse extends ServiceResponse<Hardware> {
    public HardwareOperationResponse(UUID requestId, Hardware result) {
        super.setRequestId(requestId);
        super.type = ResponseType.HardwareOperation;
        super.setPayload(result);
    }
}
