package com.vme.intellistore.hardware.commons.dtos.responses;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

/**
 * Created by Zazza on 02/05/2017.
 */
public abstract class ServiceResponse<T> {
    @Getter @Setter
    @JsonProperty("RequestGuid")
    private UUID requestId;
    @Getter @JsonProperty("Type")
    protected ResponseType type;
    @Getter @Setter
    @JsonProperty("Payload")
    private T payload;
}
