package com.vme.intellistore.hardware.commons.dtos.requests;

/**
 * Created by Zazza on 12/04/2017.
 */
public enum QueryType {
    ListAllAvailableDevices,
    ListAllAvailableDevicesForType,
    ListDevice,
    GetConfiguredDeviceInfo
}
