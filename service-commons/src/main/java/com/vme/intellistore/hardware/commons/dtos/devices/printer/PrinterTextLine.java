package com.vme.intellistore.hardware.commons.dtos.devices.printer;

import lombok.Getter;
import lombok.Setter;

import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by Mirko on 26-Jan-17.
 */
@Getter
@Setter
public class PrinterTextLine extends PrinterLine {
    private List<String> textLines;

    public PrinterTextLine() {
        lineType = PrinterLineType.TextLine;
    }

    public static PrinterTextLine convert(LinkedHashMap map){
        PrinterTextLine line = new PrinterTextLine();
        LinkedHashMap lines = (LinkedHashMap)map.get("TextLinesToPrint");
        List<String> text = (List<String>)lines.get("$values");
        line.setTextLines(text);

        return line;
    }
}
