package com.vme.intellistore.hardware.commons.dtos.devices.scale;

import lombok.Getter;

import java.text.MessageFormat;

/**
 * Created by Zazza on 07/06/2017.
 */
@Getter
public class WeightMeasurementUnits {
    private int index;
    private String name;
    private String symbol;

    public static final WeightMeasurementUnits GRAMS = new WeightMeasurementUnits(1,"Grams","g");
    public static final WeightMeasurementUnits KILOGRAMS = new WeightMeasurementUnits(2,"Kilograms","kg");
    public static final WeightMeasurementUnits OUNCES = new WeightMeasurementUnits(3,"Ounces","oz");
    public static final WeightMeasurementUnits POUNDS = new WeightMeasurementUnits(4,"Pounds","lb");

    private WeightMeasurementUnits(int index, String name, String symbol){
        this.index = index;
        this.name = name;
        this.symbol = symbol;
    }

    @Override
    public String toString() {
        return symbol;
    }

    public static WeightMeasurementUnits fromInt(int i) throws Exception{
        switch (i){
            case 1:
                return GRAMS;
            case 2:
                return KILOGRAMS;
            case 3:
                return OUNCES;
            case 4:
                return POUNDS;
            default:
                throw new Exception("Invalid Weight Unit index value " + i + " provided");
        }
    }

    public static WeightMeasurementUnits fromString(String name) throws Exception{
        String simple = name.toLowerCase().trim();

        if(simple.equals("grams")){
            return GRAMS;
        }else if(simple.equals("kilograms")){
            return KILOGRAMS;
        }else if(simple.equals("ounces")){
            return OUNCES;
        }else if(simple.equals("pounds")){
            return POUNDS;
        }else{
            throw new Exception("Invalid Weight Unit name value " + name + " provided");
        }
    }

    //    Grams(1),
//    Kilograms(2),
//    Ounces(3),
//    Pounds(4);
//
//    @Getter
//    private int value;
//
//    WeightMeasurementUnits(int value) {
//        this.value = value;
//    }
//
//    public static WeightMeasurementUnits fromInt(int value) throws Exception{
//        switch (value){
//            case 1:
//                return Grams;
//            case 2:
//                return Kilograms;
//            case 3:
//                return Ounces;
//            case 4:
//                return Pounds;
//            default:
//                throw new Exception(MessageFormat.format("Measurement unit value {0} not recognised",value));
//        }
//    }
}
