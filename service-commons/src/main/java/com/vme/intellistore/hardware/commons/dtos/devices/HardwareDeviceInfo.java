package com.vme.intellistore.hardware.commons.dtos.devices;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Map;

/**
 * Created by Zazza on 11/04/2017.
 */
@Getter
@AllArgsConstructor
public class HardwareDeviceInfo {
    @JsonProperty("DeviceType")
    private HardwareDeviceType deviceType;
    @JsonProperty("ServiceObjectName")
    private String logicalName;
    @JsonProperty("Configured")
    private Boolean configured;
    @JsonProperty("Status")
    private HardwareDeviceStatus status;
    @JsonProperty("Claimed")
    private Boolean isClaimed;
    @JsonProperty("Enabled")
    private Boolean isEnabled;
    @JsonProperty("Verified")
    private Boolean isVerified;
    @JsonProperty("DeviceProperties")
    private Map<String,Object> properties;
}
