package com.vme.intellistore.hardware.commons.dtos.requests;

/**
 * Created by Zazza on 12/04/2017.
 */
public class ConfigurationUpdateRequest extends ServiceRequest<DeviceConfigurationUpdate> {
    public ConfigurationUpdateRequest() {
        super.type = RequestType.ConfigurationUpdate;
    }
}
