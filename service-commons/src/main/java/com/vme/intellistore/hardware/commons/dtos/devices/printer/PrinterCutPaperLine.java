package com.vme.intellistore.hardware.commons.dtos.devices.printer;

import lombok.Getter;
import lombok.Setter;

import java.util.LinkedHashMap;

/**
 * Created by Mirko on 26-Jan-17.
 */
@Getter @Setter
public class PrinterCutPaperLine extends PrinterLine{
    private int cutPaperPercentage;
    private int noLinesToSkip;

    public PrinterCutPaperLine() {
        lineType = PrinterLineType.CutPaperLine;
    }

    public static PrinterCutPaperLine convert(LinkedHashMap item){
        PrinterCutPaperLine line = new PrinterCutPaperLine();

        line.setCutPaperPercentage(Integer.parseInt(item.get("CutPaperPercentage").toString()));
        line.setNoLinesToSkip(Integer.parseInt(item.get("NumberOfLinesToSkip").toString()));

        return line;
    }
}
