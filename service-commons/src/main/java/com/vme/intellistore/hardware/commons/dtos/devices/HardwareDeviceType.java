package com.vme.intellistore.hardware.commons.dtos.devices;

/**
 * Created by Mirko on 25-Jan-17.
 */
public enum HardwareDeviceType {
    Belt,
    BillAcceptor,
    BillDispenser,
    Biometrics,
    BumpBar,
    CashChanger,
    CashDrawer,
    CAT,
    CheckScanner,
    CoinAcceptor,
    CoinDispenser,
    ElectronicJournal,
    ElectronicValueRW,
    FiscalPrinter,
    Gate,
    HardTotals,
    ImageScanner,
    ItemDispenser,
    Keylock,
    Lights,
    LineDisplay,
    MICR,
    MotionSensor,
    MSR,
    Pinpad,
    PointCardRW,
    POSKeyboard,
    POSPower,
    POSPrinter,
    RemoteOrderDisplay,
    RFIDScanner,
    Scale,
    Scanner,
    SignatureCapture,
    SmartCardRW,
    ToneIndicator
}
