package com.vme.intellistore.hardware.commons.dtos.devices;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.vme.intellistore.hardware.commons.dtos.devices.printer.PrinterLine;
import com.vme.intellistore.hardware.commons.dtos.devices.printer.PrinterLines;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

/**
 * Created by Mirko on 25-Jan-17.
 */
@Getter @Setter @NoArgsConstructor(access = AccessLevel.PROTECTED)
public class PosPrinter extends Hardware{

    public static final int PrinterBitmapAsIs = -11;

    //<editor-fold desc="Properties">

    @JsonProperty("AddNewLineAfterImagePrint")
    private boolean addNewLineAfterImagePrint;
    @JsonProperty("Barcode")
    private String barcode;
    @JsonProperty("BarcodeHeight")
    private int barcodeHeight;
    @JsonProperty("BarcodeWidth")
    private int barcodeWidth;
    @JsonProperty("BarcodePosition")
    private int barcodePosition;
    @JsonProperty("BarcodeType")
    private BarcodeSymbology barcodeType;
    @JsonProperty("CharacterSet")
    private int characterSet;
    @JsonProperty("CurrencySymbol")
    private String currencySymbol;
    @JsonProperty("CurrencySymbolInAscii")
    private String currencySymbolASCII;
    @JsonProperty("CutPaperPercentage")
    private int cutPaperPercentage;
    @JsonProperty("ImageLocation")
    private String imageLocation;
    @JsonProperty("ImageWidth")
    private int imageWidth;
    @JsonProperty("NumberOfLinesToSkip")
    private int noLinesToSkip;
    @JsonProperty("PrinterLines")
    private PrinterLines printerLines;
    @JsonProperty("PrintTextOnError")
    private String printTextOnError;
    @JsonProperty("Status")
    private PosPrinterStatus status;
    @JsonProperty("TextLinesToPrint")
    private List<String> textLinesToPrint;
    @JsonProperty("TextPosition")
    private BarcodeTextPosition textPosition;

    //</editor-fold>


    //<editor-fold desc="Constructors">

    public PosPrinter(HardwareActionType actionType) {
        super(actionType,HardwareDeviceType.POSPrinter);
        this.type = "Vme.Intellistore.Hardware.EndPoint.DataTransferObjects.PosObjects.PosPrinter, Vme.Intellistore.Hardware.EndPoint.DataTransferObjects";
    }

    public PosPrinter(String actionException, Boolean actionResult, HardwareActionType actionType) {
        super(actionException, actionResult, actionType,HardwareDeviceType.POSPrinter);
        this.type = "Vme.Intellistore.Hardware.EndPoint.DataTransferObjects.PosObjects.PosPrinter, Vme.Intellistore.Hardware.EndPoint.DataTransferObjects";
    }

    @Override
    public String toString() {
        return super.toString() + "PosPrinter{" +
                "addNewLineAfterImagePrint=" + addNewLineAfterImagePrint +
                ", barcode='" + barcode + '\'' +
                ", barcodeHeight=" + barcodeHeight +
                ", barcodeWidth=" + barcodeWidth +
                ", barcodePosition=" + barcodePosition +
                ", barcodeType=" + barcodeType +
                ", characterSet=" + characterSet +
                ", currencySymbol='" + currencySymbol + '\'' +
                ", currencySymbolASCII='" + currencySymbolASCII + '\'' +
                ", cutPaperPercentage=" + cutPaperPercentage +
                ", imageLocation='" + imageLocation + '\'' +
                ", imageWidth=" + imageWidth +
                ", noLinesToSkip=" + noLinesToSkip +
                ", printerLines=" + printerLines +
                ", status=" + status +
                ", textLinesToPrint=" + textLinesToPrint +
                ", textPosition=" + textPosition +
                '}';
    }

    //</editor-fold>
}
