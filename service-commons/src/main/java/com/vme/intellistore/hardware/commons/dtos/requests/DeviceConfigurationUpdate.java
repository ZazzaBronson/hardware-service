package com.vme.intellistore.hardware.commons.dtos.requests;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.vme.intellistore.hardware.commons.dtos.devices.HardwareDeviceType;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Zazza on 17/04/2017.
 */
@Getter @Setter @AllArgsConstructor
public class DeviceConfigurationUpdate {
    @JsonProperty("DeviceType")
    private HardwareDeviceType type;
    @JsonProperty("DeviceServiceName")
    private String deviceLogicalName;
}
