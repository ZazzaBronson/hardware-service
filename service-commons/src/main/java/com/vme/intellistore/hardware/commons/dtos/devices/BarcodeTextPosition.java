package com.vme.intellistore.hardware.commons.dtos.devices;

import lombok.Getter;

/**
 * @author Mirko
 * @since 16-Feb-17.
 */
public enum BarcodeTextPosition {
    Below(-13),
    Above(-12),
    None(-11);

    @Getter
    private int numVal;

    BarcodeTextPosition(int numVal) {
        this.numVal = numVal;
    }
}
