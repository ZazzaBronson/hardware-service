package com.vme.intellistore.hardware.commons.dtos.devices;

import lombok.Getter;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Mirko on 25-Jan-17.
 */
public enum BarcodeSymbology {
    Unknown(0),
    Upca(101),
    Upce(102),
    EanJan8(103),
    EanJan13(104),
    TF(105),
    Itf(106),
    Codabar(107),
    Code39(108),
    Code93(109),
    Code128(110),
    Upcas(111),
    Upces(112),
    Upcd1(113),
    Upcd2(114),
    Upcd3(115),
    Upcd4(116),
    Upcd5(117),
    Ean8S(118),
    Ean13S(119),
    Ean128(120),
    Ocra(121),
    Ocrb(122),
    Code128Parsed(123),
    Gs1DataBar(131),
    Rss14(131),
    Gs1DataBarExpanded(132),
    RssExpanded(132),
    Gs1DataBarStackedOmnidirectional(133),
    Gs1DataBarExpandedStacked(134),
    Cca(151),
    Ccb(152),
    Ccc(153),
    Pdf417(201),
    Maxicode(202),
    DataMatrix(203),
    QRCode(204),
    MicroQRCode(205),
    Aztec(206),
    MicroPDF417(207),
    Other(501);

    @Getter
    private int numVal;

    private static Map<Integer,BarcodeSymbology> map = new HashMap<Integer,BarcodeSymbology>();

    static {
        for(BarcodeSymbology symbology : BarcodeSymbology.values()){
            map.put(symbology.numVal,symbology);
        }
    }

    BarcodeSymbology(final int numVal) {
        this.numVal = numVal;
    }

    public static BarcodeSymbology tryParse(int value){
        return map.get(value);
    }
}
