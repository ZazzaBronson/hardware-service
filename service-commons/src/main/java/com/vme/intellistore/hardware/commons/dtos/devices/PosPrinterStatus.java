package com.vme.intellistore.hardware.commons.dtos.devices;

import lombok.Getter;

/**
 * @author Mirko
 * @since 16-Feb-17.
 */
public enum PosPrinterStatus {
    PrinterBitmapAsIs(-11),
    PrinterCutPaperFullCut(100),
    PrinterBitmapLeft(-1),
    PrinterBitmapCenter(-2),
    PrinterBitmapRight(-3),
    PrinterBarCodeLeft(-1),
    PrinterBarCodeCenter(-2),
    PrinterBarCodeRight(-3),
    CharacterSetUnicode(997),
    CharacterSetAscii(998),
    CharacterSetAnsi(999),
    ExtendedErrorCoverOpen(201),
    ExtendedErrorJournalEmpty(202),
    ExtendedErrorReceiptEmpty(203),
    ExtendedErrorSlipEmpty(204),
    ExtendedErrorSlipForm(205),
    ExtendedErrorTooBig(206),
    ExtendedErrorBadFormat(207),
    ExtendedErrorJournalCartridgeRemoved(208),
    ExtendedErrorJournalCartridgeEmpty(209),
    ExtendedErrorJournalHeadCleaning(210),
    ExtendedErrorReceiptCartridgeRemoved(211),
    ExtendedErrorReceiptCartridgeEmpty(212),
    ExtendedErrorReceiptHeadCleaning(213),
    ExtendedErrorSlipCartridgeRemoved(214),
    ExtendedErrorSlipCartridgeEmpty(215),
    ExtendedErrorSlipHeadCleaning(216),
    StatusCoverOpen(11),
    StatusCoverOK(12),
    StatusJournalEmpty(21),
    StatusJournalNearEmpty(22),
    StatusJournalPaperOK(23),
    StatusReceiptEmpty(24),
    StatusReceiptNearEmpty(25),
    StatusReceiptPaperOK(26),
    StatusSlipEmpty(27),
    StatusSlipNearEmpty(28),
    StatusSlipPaperOK(29),
    StatusJournalCartridgeEmpty(41),
    StatusJournalCartridgeNearEmpty(42),
    StatusJournalHeadCleaning(43),
    StatusJournalCartridgeOK(44),
    StatusReceiptCartridgeEmpty(45),
    StatusReceiptCartridgeNearEmpty(46),
    StatusReceiptHeadCleaning(47),
    StatusReceiptCartridgeOK(48),
    StatusSlipCartridgeEmpty(49),
    StatusSlipCartridgeNearEmpty(50),
    StatusSlipHeadCleaning(51),
    StatusSlipCartridgeOK(52),
    StatusJournalCoverOpen(60),
    StatusJournalCoverOK(61),
    StatusReceiptCoverOpen(62),
    StatusReceiptCoverOK(63),
    StatusSlipCoverOpen(64),
    StatusSlipCoverOK(65),
    StatusIdle(1001);

    @Getter
    private int numVal;

    PosPrinterStatus(int numVal) {
        this.numVal = numVal;
    }
}
