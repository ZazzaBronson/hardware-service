package com.vme.intellistore.hardware.commons.dtos.responses;

import com.vme.intellistore.hardware.commons.dtos.devices.HardwareDeviceInfo;
import com.vme.intellistore.hardware.commons.dtos.devices.HardwareDeviceType;

import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Created by Zazza on 02/05/2017.
 */
public class QueryResponse extends ServiceResponse<Map<HardwareDeviceType,List<HardwareDeviceInfo>>> {

    public QueryResponse(UUID requestId, Map<HardwareDeviceType,List<HardwareDeviceInfo>> result) {
        super.setRequestId(requestId);
        super.type = ResponseType.Query;
        super.setPayload(result);
    }
}
