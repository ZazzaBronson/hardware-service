package com.vme.intellistore.hardware.commons.dtos.devices.scale;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Zazza on 07/06/2017.
 */
@Getter @Setter @AllArgsConstructor
public class ScaleFunction {
    @JsonProperty("FunctionType")
    private ScaleFunctionType functionType;
    @JsonProperty("FunctionParameter")
    private Object functionParameter;
}
