package com.vme.intellistore.hardware.commons.dtos.requests;

/**
 * Created by Zazza on 12/04/2017.
 */
public class QueryRequest extends ServiceRequest<Query> {

    public QueryRequest() {
        super.type = RequestType.Query;
    }

}
