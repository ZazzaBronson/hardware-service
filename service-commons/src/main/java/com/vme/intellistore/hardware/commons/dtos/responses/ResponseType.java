package com.vme.intellistore.hardware.commons.dtos.responses;

/**
 * Created by Zazza on 02/05/2017.
 */
public enum ResponseType {
    Query,
    HardwareOperation,
    ConfigurationUpdate
}
