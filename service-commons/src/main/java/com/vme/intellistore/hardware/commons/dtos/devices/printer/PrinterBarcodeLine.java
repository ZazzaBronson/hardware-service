package com.vme.intellistore.hardware.commons.dtos.devices.printer;

import com.vme.intellistore.hardware.commons.dtos.devices.BarcodeSymbology;
import com.vme.intellistore.hardware.commons.dtos.devices.BarcodeTextPosition;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.LinkedHashMap;

/**
 * Created by Mirko on 26-Jan-17.
 */
@Getter
@Setter
public class PrinterBarcodeLine extends PrinterLine {
    private String barcode;
    private int height;
    private int width;
    private int position;
    private BarcodeSymbology type;
    private BarcodeTextPosition textPosition;

    public PrinterBarcodeLine() {
        lineType = PrinterLineType.BarcodeLine;
    }

    public PrinterBarcodeLine(String barcode, int height, int width, int position, BarcodeSymbology type, BarcodeTextPosition textPosition) {
        lineType = PrinterLineType.BarcodeLine;
        this.barcode = barcode;
        this.height = height;
        this.width = width;
        this.position = position;
        this.type = type;
        this.textPosition = textPosition;
    }

    public static PrinterBarcodeLine convert(LinkedHashMap item){
        PrinterBarcodeLine line = new PrinterBarcodeLine();
        line.setBarcode((String)item.get("Barcode"));
        line.setHeight((int)item.get("BarcodeHeight"));
        line.setPosition((int)item.get("BarCodePosition"));
        line.setType(BarcodeSymbology.valueOf(item.get("BarCodeType").toString()));
        line.setWidth((int)item.get("BarcodeWidth"));
        line.setTextPosition(BarcodeTextPosition.valueOf((String)item.get("TextPosition")));

        return line;
    }
}
