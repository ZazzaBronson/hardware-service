package com.vme.intellistore.hardware.commons.dtos.devices.printer;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;

/**
 * Created by Mirko on 26-Jan-17.
 */
public class PrinterLine{
    @Getter
    @JsonProperty("LineType")
    protected PrinterLineType lineType;

    protected PrinterLine() {
    }
}
