package com.vme.intellistore.hardware.commons.dtos.devices;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Arrays;

/**
 * Created by Mirko on 25-Jan-17.
 */
@Getter @Setter @NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Msr extends Hardware {

    //<editor-fold desc="Properties">

    @JsonProperty("AccountNumber")
    private String accountNumber;
    @JsonProperty("ExpirationDate")
    private String expirationDate;
    @JsonProperty("FirstName")
    private String firstName;
    @JsonProperty("MiddleInitial")
    private String middleName;
    @JsonProperty("ServiceCode")
    private String serviceCode;
    @JsonProperty("Status")
    private int status;
    @JsonProperty("Suffix")
    private String suffix;
    @JsonProperty("Surname")
    private String surname;
    @JsonProperty("Title")
    private String title;
    @JsonProperty("Track1Data")
    private byte[] track1Data;
    @JsonProperty("Track2Data")
    private byte[] track2Data;

    //</editor-fold>

    //<editor-fold desc="Constructors">

    public Msr(HardwareActionType actionType) {
        super(actionType,HardwareDeviceType.MSR);
        this.type = "Vme.Intellistore.Hardware.EndPoint.DataTransferObjects.PosObjects.Msr, Vme.Intellistore.Hardware.EndPoint.DataTransferObjects";
    }

    public Msr(String actionException, Boolean actionResult, HardwareActionType actionType) {
        super(actionException, actionResult, actionType,HardwareDeviceType.MSR);
        this.type = "Vme.Intellistore.Hardware.EndPoint.DataTransferObjects.PosObjects.Msr, Vme.Intellistore.Hardware.EndPoint.DataTransferObjects";
    }

    @Override
    public String toString() {
        return super.toString() + "Msr{" +
                "accountNumber='" + accountNumber + '\'' +
                ", expirationDate='" + expirationDate + '\'' +
                ", firstName='" + firstName + '\'' +
                ", middleName='" + middleName + '\'' +
                ", serviceCode='" + serviceCode + '\'' +
                ", status=" + status +
                ", suffix='" + suffix + '\'' +
                ", surname='" + surname + '\'' +
                ", title='" + title + '\'' +
                ", track1Data=" + Arrays.toString(track1Data) +
                ", track2Data=" + Arrays.toString(track2Data) +
                '}';
    }

    //</editor-fold>
}
