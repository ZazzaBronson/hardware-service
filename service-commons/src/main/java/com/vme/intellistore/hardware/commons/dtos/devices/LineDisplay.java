package com.vme.intellistore.hardware.commons.dtos.devices;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.vme.intellistore.hardware.commons.TextAlignment;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by Mirko on 25-Jan-17.
 */
@Getter @Setter @NoArgsConstructor(access = AccessLevel.PROTECTED)
public class LineDisplay extends Hardware {

    //<editor-fold desc="Properties">

    @JsonProperty("CharacterSet")
    private int characterSet;
    @JsonProperty("CurrencySymbol")
    private String currencySymbol;
    @JsonProperty("CurrencySymbolInAscii")
    private int currencySymbolASCII;
    @JsonProperty("CurrencySymbolOverride")
    private String currencySymbolOverride;
    @JsonProperty("LineDisplayLength")
    private int lineDisplayLength;
    @JsonProperty("RowIndex")
    private int rowIndex;
    @JsonProperty("TextAlignment")
    private TextAlignment textAlignment;
    @JsonProperty("TextToDisplay")
    private String textToDisplay;

    //</editor-fold>

    //<editor-fold desc="Constructors">

    public LineDisplay(HardwareActionType actionType) {
        super(actionType,HardwareDeviceType.LineDisplay);
        this.type = "Vme.Intellistore.Hardware.EndPoint.DataTransferObjects.PosObjects.LineDisplay, Vme.Intellistore.Hardware.EndPoint.DataTransferObjects";
    }

    public LineDisplay(String actionException, Boolean actionResult, HardwareActionType actionType) {
        super(actionException, actionResult, actionType,HardwareDeviceType.LineDisplay);
        this.type = "Vme.Intellistore.Hardware.EndPoint.DataTransferObjects.PosObjects.LineDisplay, Vme.Intellistore.Hardware.EndPoint.DataTransferObjects";
    }

    @Override
    public String toString() {
        return super.toString() + "LineDisplay{" +
                "characterSet=" + characterSet +
                ", currencySymbol='" + currencySymbol + '\'' +
                ", currencySymbolASCII=" + currencySymbolASCII +
                ", currencySymbolOverride='" + currencySymbolOverride + '\'' +
                ", lineDisplayLength=" + lineDisplayLength +
                ", rowIndex=" + rowIndex +
                ", textAlignment=" + textAlignment +
                ", textToDisplay='" + textToDisplay + '\'' +
                '}';
    }

    //</editor-fold>
}
