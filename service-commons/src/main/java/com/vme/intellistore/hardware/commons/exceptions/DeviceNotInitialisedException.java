package com.vme.intellistore.hardware.commons.exceptions;

/**
 * Created by Mirko on 25-Jan-17.
 */
public class DeviceNotInitialisedException extends Exception {
    public DeviceNotInitialisedException() {
    }

    public DeviceNotInitialisedException(String message) {
        super(message);
    }

    public DeviceNotInitialisedException(String message, Throwable cause) {
        super(message, cause);
    }

    public DeviceNotInitialisedException(Throwable cause) {
        super(cause);
    }

    public DeviceNotInitialisedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
