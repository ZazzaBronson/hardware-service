package com.vme.intellistore.hardware.commons.dtos.devices;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Arrays;

/**
 * Created by Mirko on 25-Jan-17.
 */
@Getter @Setter @NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Scanner extends Hardware {

    //<editor-fold desc="Properties">

    @JsonProperty("Data")
    private byte[] data;

    @JsonProperty("DataString")
    public String dataString;// = this.data == null ? "" : new String(this.data,Charset.forName("UTF-8"));

    //</editor-fold>

    //<editor-fold desc="Constructors">

    public Scanner(HardwareActionType actionType) {
        super(actionType,HardwareDeviceType.Scanner);
        this.type = "Vme.Intellistore.Hardware.EndPoint.DataTransferObjects.PosObjects.Scanner, Vme.Intellistore.Hardware.EndPoint.DataTransferObjects";
    }

    public Scanner(String actionException, Boolean actionResult, HardwareActionType actionType) {
        super(actionException, actionResult, actionType, HardwareDeviceType.Scanner);
        this.type = "Vme.Intellistore.Hardware.EndPoint.DataTransferObjects.PosObjects.Scanner, Vme.Intellistore.Hardware.EndPoint.DataTransferObjects";
    }

    //</editor-fold>

    @Override
    public String toString() {
        return super.toString() + "Scanner{" +
                "data=" + Arrays.toString(data) +
                ", dataString='" + dataString + '\'' +
                '}';
    }
}
