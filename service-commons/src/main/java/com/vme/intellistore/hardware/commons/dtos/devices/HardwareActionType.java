package com.vme.intellistore.hardware.commons.dtos.devices;

/**
 * Created by Mirko on 25-Jan-17.
 */
public enum HardwareActionType {
    Claim,
    Claimed,
    Close,
    Closed,
    Enable,
    Enabled,
    Disable,
    Disabled,
    Initialise,
    Initialised,
    Open,
    Opened,
    Release,
    Released,
    SendData,//Client to Device
    SentData,
    ReceivedData,//Device to Client
    StatusUpdated//Device to Client
}
