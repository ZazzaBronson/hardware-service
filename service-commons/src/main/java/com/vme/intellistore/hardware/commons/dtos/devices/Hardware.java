package com.vme.intellistore.hardware.commons.dtos.devices;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.NotImplementedException;

import java.text.MessageFormat;

/**
 * Created by Mirko on 25-Jan-17.
 */
@Slf4j
@Getter
@JsonPropertyOrder({"type","objectType","actionType","actionResult","actionException"})
public abstract class Hardware {

    //<editor-fold desc="Properties">

    @JsonProperty("$type")
    protected String type;

    @Setter @JsonProperty("ObjectServiceName")
    private String logicalName = "";

    @Setter @JsonProperty("ActionException")
    private String actionException;
    @Setter @JsonProperty("ActionResult")
    private Boolean actionResult;
    @Setter @JsonProperty("ActionType")
    private HardwareActionType actionType;
    @JsonProperty("ObjectType")
    private HardwareDeviceType objectType;
    @Setter @JsonProperty("ObjectStatus")
    private HardwareDeviceStatus objectStatus;

    //</editor-fold>

    //<editor-fold desc="Constructors">

    protected Hardware(){
    }

    protected Hardware(HardwareActionType actionType, HardwareDeviceType objectType) {
        this.actionType = actionType;
        this.objectType = objectType;
    }

    protected Hardware(String actionException, Boolean actionResult, HardwareActionType actionType, HardwareDeviceType objectType) {
        this.actionException = actionException;
        this.actionResult = actionResult;
        this.actionType = actionType;
        this.objectType = objectType;
    }

    public Hardware(String actionException, Boolean actionResult, HardwareActionType actionType) {
        this.actionException = actionException;
        this.actionResult = actionResult;
        this.actionType = actionType;
    }

    public Hardware(String type, String logicalName, String actionException, Boolean actionResult, HardwareActionType actionType, HardwareDeviceType objectType, HardwareDeviceStatus objectStatus) {
        this.type = type;
        this.logicalName = logicalName;
        this.actionException = actionException;
        this.actionResult = actionResult;
        this.actionType = actionType;
        this.objectType = objectType;
        this.objectStatus = objectStatus;
    }

    //</editor-fold>

    public static Hardware createHardwareObject(HardwareDeviceType deviceType, HardwareActionType actionType){
        switch(deviceType){
            case Scanner:
                return new Scanner(actionType);
            case POSPrinter:
                return new PosPrinter(actionType);
            case LineDisplay:
                return new LineDisplay(actionType);
            case CashDrawer:
                return new CashDrawer(actionType);
            case MSR:
                return new Msr(actionType);
            case Scale:
                return new Scale(actionType);
            default:
                log.error(MessageFormat.format("Hardware->createHardwareObject: {0} is not implemented",deviceType));
                throw new NotImplementedException();
        }
    }

    @Override
    public String toString() {
        return "Hardware{" +
                "actionException=" + actionException +
                ", actionResult=" + actionResult +
                ", actionType=" + actionType +
                ", objectType=" + objectType +
                ", objectStatus=" + objectStatus +
                '}';
    }
}
