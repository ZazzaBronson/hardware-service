package com.vme.intellistore.hardware.commons.dtos.devices;

import lombok.Getter;

/**
 * Created by Mirko on 25-Jan-17.
 */
public enum CashDrawerStatus {
    Open(1),
    Closed(0);

    @Getter
    private int numVal;

    CashDrawerStatus(int numVal) {
        this.numVal = numVal;
    }
}
