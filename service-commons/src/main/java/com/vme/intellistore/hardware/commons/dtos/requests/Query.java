package com.vme.intellistore.hardware.commons.dtos.requests;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Created by Zazza on 13/04/2017.
 */
@Getter
@AllArgsConstructor
public class Query {
    @JsonProperty("Type")
    private QueryType type;
    @JsonProperty("Parameter")
    private String parameter;
}
