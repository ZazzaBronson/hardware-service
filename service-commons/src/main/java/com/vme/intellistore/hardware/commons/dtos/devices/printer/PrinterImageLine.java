package com.vme.intellistore.hardware.commons.dtos.devices.printer;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by Mirko on 26-Jan-17.
 */
@Getter
@Setter
public class PrinterImageLine extends PrinterLine {
    private boolean addNewLineAfter;
    private String imageLocation;
    private int width;

    public PrinterImageLine() {
        lineType = PrinterLineType.ImageLine;
    }
}
