package com.vme.intellistore.hardware.commons.exceptions;

/**
 * Created by Zazza on 09/05/2017.
 */
public class DeviceNotImplementedException extends Exception {
    public DeviceNotImplementedException() {
    }

    public DeviceNotImplementedException(String message) {
        super(message);
    }

    public DeviceNotImplementedException(String message, Throwable cause) {
        super(message, cause);
    }

    public DeviceNotImplementedException(Throwable cause) {
        super(cause);
    }

    public DeviceNotImplementedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
