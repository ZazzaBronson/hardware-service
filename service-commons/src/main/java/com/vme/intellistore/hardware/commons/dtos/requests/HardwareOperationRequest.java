package com.vme.intellistore.hardware.commons.dtos.requests;

import com.vme.intellistore.hardware.commons.dtos.devices.Hardware;

/**
 * Created by Zazza on 12/04/2017.
 */
public class HardwareOperationRequest extends ServiceRequest<Hardware> {
    public HardwareOperationRequest() {
        super.type = RequestType.HardwareOperation;
    }
}
