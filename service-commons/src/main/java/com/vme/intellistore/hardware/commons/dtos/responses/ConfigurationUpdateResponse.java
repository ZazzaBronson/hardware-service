package com.vme.intellistore.hardware.commons.dtos.responses;

import java.util.UUID;

/**
 * Created by Zazza on 02/05/2017.
 */
public class ConfigurationUpdateResponse extends ServiceResponse<Boolean> {
    public ConfigurationUpdateResponse(UUID requestId, Boolean result) {
        super.setRequestId(requestId);
        super.type = ResponseType.ConfigurationUpdate;
        super.setPayload(result);
    }
}
