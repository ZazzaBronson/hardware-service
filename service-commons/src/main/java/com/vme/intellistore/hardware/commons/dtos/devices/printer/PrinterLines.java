package com.vme.intellistore.hardware.commons.dtos.devices.printer;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * Created by Zazza on 13/06/2017.
 */
@Getter @Setter
public class PrinterLines {
    @JsonProperty("$values")
    private List<Object> values;
}
