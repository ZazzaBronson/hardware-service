package com.vme.intellistore.hardware.commons.dtos.requests;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;

import java.util.UUID;

/**
 * Created by Zazza on 12/04/2017.
 */
public abstract class ServiceRequest<T> {
    @Getter @Setter @JsonProperty("Guid")
    private UUID id;
    @Getter @JsonProperty("Type")
    protected RequestType type;
    @Getter @Setter @JsonProperty("Payload")
    private T payload;
}
