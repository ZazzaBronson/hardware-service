package com.vme.intellistore.hardware.commons.dtos.devices;

import lombok.Getter;

/**
 * Created by Zazza on 11/04/2017.
 */
public enum HardwareDeviceStatus {
    Closed(1),
    Idle(2),
    Busy(3),
    Error(4);

    @Getter
    private final int value;

    HardwareDeviceStatus(int value) {
        this.value = value;
    }

    public static HardwareDeviceStatus fromInt(int value){
        switch (value){
            case 1:
                return Closed;
            case 2:
                return Idle;
            case 3:
                return Busy;
            case 4:
                return Error;
            default:
                return Error;
        }
    }
}
