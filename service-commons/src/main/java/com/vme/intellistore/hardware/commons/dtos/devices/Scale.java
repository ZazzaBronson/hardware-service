package com.vme.intellistore.hardware.commons.dtos.devices;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.vme.intellistore.hardware.commons.dtos.devices.scale.ScaleFunction;
import com.vme.intellistore.hardware.commons.dtos.devices.scale.WeightMeasurementUnits;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Created by Mirko on 25-Jan-17.
 */
@Getter @Setter @NoArgsConstructor( access = AccessLevel.PROTECTED )
public class Scale extends Hardware {

    //<editor-fold desc="Properties">

    @JsonProperty("Async")
    private boolean isAsync;
    @JsonProperty("Unit")
    private WeightMeasurementUnits units;
    @JsonProperty("Weight")
    private int weight;
    @JsonProperty("Function")
    private ScaleFunction function;

    //</editor-fold>

    //<editor-fold desc="Constructors">

    public Scale(HardwareActionType actionType) {
        super(actionType,HardwareDeviceType.Scale);
        this.type = "Vme.Intellistore.Hardware.EndPoint.DataTransferObjects.PosObjects.Scale, Vme.Intellistore.Hardware.EndPoint.DataTransferObjects";
    }

    public Scale(String actionException, Boolean actionResult, HardwareActionType actionType) {
        super(actionException, actionResult, actionType,HardwareDeviceType.Scale);
        this.type = "Vme.Intellistore.Hardware.EndPoint.DataTransferObjects.PosObjects.Scale, Vme.Intellistore.Hardware.EndPoint.DataTransferObjects";
    }

    //</editor-fold>


    @Override
    public String toString() {
        return super.toString() + "Scale{ isAsync=" + isAsync +
                ", units=" + units +
                ", weight=" + weight +
                ", " +
                "}";

        //action={type='" + function.getFunctionType().toString() +"', parameter='" + function.getFunctionParameter().toString() + "'}
    }
}
