package com.vme.intellistore.hardware.commons.dtos.devices.printer;

/**
 * Created by Zazza on 09/06/2017.
 */
public enum PrinterLineType {
    BarcodeLine,
    CutPaperLine,
    ImageLine,
    TextLine
}
