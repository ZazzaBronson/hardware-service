package com.vme.intellistore.hardware.service.test.unit.services;

import com.vme.intellistore.hardware.commons.dtos.devices.*;
import com.vme.intellistore.hardware.commons.dtos.responses.HardwareOperationResponse;
import com.vme.intellistore.hardware.managers.*;
import com.vme.intellistore.hardware.processors.TcpMessageProcessor;
import com.vme.intellistore.hardware.service.test.TestConfiguration;
import com.vme.intellistore.hardware.services.PosHardwareService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import java.util.UUID;

/**
 * @author Mirko
 * @since 07-Feb-17.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestConfiguration.class)
public class PosHardwareServiceTest {

    //<editor-fold desc="Fields">

    @Autowired
    @InjectMocks
    private PosHardwareService hardwareService;

    @Mock
    TcpMessageProcessor tcpMessageProcessor;

    @Mock
    CashDrawerServiceManager cashDrawerServiceManager;

    @Mock
    LineDisplayServiceManager lineDisplayServiceManager;

    @Mock
    MsrServiceManager msrServiceManager;

    @Mock
    PosPrinterServiceManager posPrinterServiceManager;

    @Mock
    ScaleServiceManager scaleServiceManager;

    @Mock
    ScannerServiceManager scannerServiceManager;

    Hardware device;

    //</editor-fold>

    @Before
    public void setup(){
        MockitoAnnotations.initMocks(this);
    }


    @Test(expected = IllegalArgumentException.class)
    public void requestDeviceAction_nullParam_Test(){
        hardwareService.requestDeviceAction(UUID.randomUUID(),null);
    }

//    @Test(expected = IllegalArgumentException.class)
//    public void requestDeviceAction_nullObjectType_Test(){
//        Hardware device = new Hardware(HardwareActionType.Open);
//        hardwareService.requestDeviceAction(device);
//    }
//
//    @Test(expected = IllegalArgumentException.class)
//    public void requestDeviceAction_nullActionType_Test(){
//        Hardware device = new Hardware(null);
//        hardwareService.requestDeviceAction(device);
//    }

    @Test
    public void requestDeviceAction_CashDrawerOpen_Test(){
        Hardware device = new CashDrawer(HardwareActionType.Open);
        HardwareOperationResponse result = hardwareService.requestDeviceAction(UUID.randomUUID(),device);

        Assert.notNull(result);
        Assert.isTrue(result.getPayload().getActionResult());
        Assert.isNull(result.getPayload().getActionException());
        Assert.isInstanceOf(CashDrawer.class,result);
    }

    @Test
    public void requestDeviceAction_LineDisplayOpen_Test(){
        Hardware device = new LineDisplay(HardwareActionType.Open);
        HardwareOperationResponse result = hardwareService.requestDeviceAction(UUID.randomUUID(),device);

        Assert.notNull(result);
        Assert.isTrue(result.getPayload().getActionResult());
        Assert.isNull(result.getPayload().getActionException());
        Assert.isInstanceOf(LineDisplay.class,result);
    }

    @Test
    public void requestDeviceAction_MsrOpen_Test(){
        Hardware device = new Msr(HardwareActionType.Open);
        HardwareOperationResponse result = hardwareService.requestDeviceAction(UUID.randomUUID(),device);

        Assert.notNull(result);
        Assert.isTrue(result.getPayload().getActionResult());
        Assert.isNull(result.getPayload().getActionException());
        Assert.isInstanceOf(Msr.class,result);
    }

    @Test
    public void requestDeviceAction_PosPrinterOpen_Test(){
        Hardware device = new PosPrinter(HardwareActionType.Open);
        HardwareOperationResponse result = hardwareService.requestDeviceAction(UUID.randomUUID(),device);

        Assert.notNull(result);
        Assert.isTrue(result.getPayload().getActionResult());
        Assert.isNull(result.getPayload().getActionException());
        Assert.isInstanceOf(PosPrinter.class,result);
    }

    @Test
    public void requestDeviceAction_ScaleOpen_Test(){
        Hardware device = new Scale(HardwareActionType.Open);
        HardwareOperationResponse result = hardwareService.requestDeviceAction(UUID.randomUUID(),device);

        Assert.notNull(result);
        Assert.isTrue(result.getPayload().getActionResult());
        Assert.isNull(result.getPayload().getActionException());
        Assert.isInstanceOf(Scale.class,result);
    }

    @Test
    public void requestDeviceAction_ScannerOpen_Test(){
        Hardware device = new Scanner(HardwareActionType.Open);
        HardwareOperationResponse result = hardwareService.requestDeviceAction(UUID.randomUUID(),device);

        Assert.notNull(result);
        Assert.isTrue(result.getPayload().getActionResult());
        Assert.isNull(result.getPayload().getActionException());
        Assert.isInstanceOf(Scanner.class,result);
    }
}
