package com.vme.intellistore.hardware.service.test;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Created by Mirko on 07-Feb-17.
 */
@ComponentScan(basePackages = "com.vme.intellistore.hardware")
@Configuration
public class TestConfiguration {
}
