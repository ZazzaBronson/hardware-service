package com.vme.intellistore.hardware.api;

import com.vme.intellistore.hardware.commons.dtos.devices.HardwareDeviceType;
import com.vme.intellistore.hardware.commons.dtos.devices.Msr;
import com.vme.intellistore.hardware.configuration.DeviceConfiguration;
import com.vme.intellistore.hardware.managers.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;

@RestController
@RequestMapping(value = "/testing", produces = MediaType.APPLICATION_JSON_VALUE)
@Slf4j
public class TestingController {

    @Autowired
    private DeviceConfiguration deviceConfiguration;

    @Autowired
    private ServiceManagerHelper serviceManagerHelper;

    private HttpHeaders getHeaders(){
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        return headers;
    }

    @RequestMapping(value = "/scanner/scan", method = RequestMethod.POST)
    public ResponseEntity<Boolean> triggerScanBarcode(@RequestParam String barcode, @RequestParam(required = false) String deviceName) throws RuntimeException{

        try {
            if(StringUtils.isEmpty(barcode)){
                throw new IllegalArgumentException("Parameter barcode cannot be emtpy");
            }

            if (StringUtils.isEmpty(deviceName)) {
                deviceName = deviceConfiguration.getNameScanner();
            }

            ScannerServiceManager manager = (ScannerServiceManager) serviceManagerHelper.getInstance(HardwareDeviceType.Scanner, deviceName);
            manager.triggerBarcodeScan(barcode);
        }catch (Exception e){
            log.error("An exception occurred in Testing REST Api -> triggerScanBarcode.",e);
            throw new RuntimeException(e.getMessage());
        }

        return new ResponseEntity<Boolean>(true, HttpStatus.OK);
    }

    @RequestMapping(value = "/scale/set_value", method = RequestMethod.PUT)
    public ResponseEntity<Boolean> setScaleWeight(@RequestParam Double weightValue, @RequestParam(required = false) String deviceName) throws RuntimeException{
        try{
            if(weightValue == null){
                throw new IllegalArgumentException("Parameter weightValue cannot be empty");
            }

            if(StringUtils.isEmpty(deviceName)){
                deviceName = deviceConfiguration.getNameScale();
            }

            ScaleServiceManager serviceManager = (ScaleServiceManager) serviceManagerHelper.getInstance(HardwareDeviceType.Scale,deviceName);
            serviceManager.setScaleWeight(weightValue);

        }catch (Exception e){
            log.error("An exception occurred in Testing REST Api -> setScaleWeight",e);
            throw new RuntimeException(e.getMessage());
        }

        return new ResponseEntity<Boolean>(true,HttpStatus.OK);
    }

    @RequestMapping(value = "/msr/scan", method = RequestMethod.POST)
    public ResponseEntity<Boolean> triggerMsrScan(@RequestBody Msr msr, @RequestParam(required = false) String deviceName){
        try {
            Assert.notNull(msr, "Parameter msr cannot be null");

            if(StringUtils.isEmpty(deviceName)){
                deviceName = deviceConfiguration.getNameMsr();
            }

            MsrServiceManager serviceManager = (MsrServiceManager) serviceManagerHelper.getInstance(HardwareDeviceType.MSR,deviceName);
            serviceManager.triggerMsrScan(msr);
        }catch (Exception e){
            log.error("An exception occurred in Testing REST Api -> triggerMsrScan",e);
            throw new RuntimeException(e.getMessage());
        }

        return new ResponseEntity<Boolean>(true,HttpStatus.OK);
    }

    @RequestMapping(value = "/linedisplay/get_text", method = RequestMethod.GET)
    public ResponseEntity<Object> getLineDisplayData(@RequestParam(required = false) String deviceName){
        String data = "";

        try{
            if(StringUtils.isEmpty(deviceName)){
                deviceName = deviceConfiguration.getNameLineDisplay();
            }

            LineDisplayServiceManager serviceManager = (LineDisplayServiceManager)serviceManagerHelper.getInstance(HardwareDeviceType.LineDisplay,deviceName);
            data = serviceManager.getLineDisplayData();
        }catch (Exception e){
            log.error("An exception occurred in Testing REST Api -> getLineDisplayData",e);
            throw new RuntimeException(e.getMessage());
        }

        HashMap<String,String> result = new HashMap<>();
        result.put("data",data);
        return new ResponseEntity<Object>(result, this.getHeaders(), HttpStatus.OK);
    }

    @RequestMapping(value = "/posprinter/get_text", method = RequestMethod.GET)
    public ResponseEntity<Object> getPosPrinterData(@RequestParam(required = false) String deviceName){
        String data = "";

        try{
            if(StringUtils.isEmpty(deviceName)){
                deviceName = deviceConfiguration.getNamePosPrinter();
            }

            PosPrinterServiceManager serviceManager = (PosPrinterServiceManager) serviceManagerHelper.getInstance(HardwareDeviceType.POSPrinter,deviceName);
            data = serviceManager.getTextData();
        }catch (Exception e){
            log.error("An exception occurred in Testing REST Api -> getPosPrinterData",e);
            throw new RuntimeException(e.getMessage());
        }

        HashMap<String,String> result = new HashMap<>();
        result.put("data",data);
        return new ResponseEntity<Object>(result, this.getHeaders(), HttpStatus.OK);
    }
}
