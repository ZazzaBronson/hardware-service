package com.vme.intellistore.hardware.managers;

import com.vme.intellistore.hardware.commons.dtos.devices.Hardware;
import com.vme.intellistore.hardware.commons.dtos.devices.HardwareActionType;
import com.vme.intellistore.hardware.commons.dtos.devices.scale.WeightMeasurementUnits;
import com.vme.intellistore.hardware.configuration.BaseConfiguration;
import com.vme.intellistore.hardware.configuration.DeviceConfiguration;
import com.vme.intellistore.hardware.helpers.ExecutionHelper;
import com.vme.intellistore.javapos.device.simulator.testing.beans.SimulatedTestingScale;
import com.vme.intellistore.javapos.device.simulator.util.ExecutionState;
import jpos.JposException;
import jpos.Scale;
import jpos.events.DataEvent;
import jpos.events.DataListener;
import jpos.events.ErrorEvent;
import jpos.events.ErrorListener;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.annotation.PostConstruct;
import java.text.MessageFormat;

/**
 * Created by Mirko on 27-Jan-17.
 */
@Component
@Slf4j
@Scope("prototype")
public class ScaleServiceManager extends HardwareServiceManager implements DataListener,ErrorListener {

    @Autowired
    private DeviceConfiguration deviceConfiguration;

    //<editor-fold desc="Constructors">

    protected ScaleServiceManager(){
        this.setDeviceType("Scale");
    }

    @PostConstruct
    private void init(){
        this.setPosDevice(executionHelper.isTest() ? new SimulatedTestingScale() : new Scale());
    }

//    protected ScaleServiceManager() {
//        this.setDeviceType("Scale");
//        this.setPosDevice(new Scale());
//    }
//
//    protected ScaleServiceManager(String logicalName, DeviceConfiguration deviceConfiguration){
//        this.setDeviceType("Scale");
//        this.setPosDevice(new Scale());
//        this.setDeviceLogicalName(logicalName);
//        this.deviceConfiguration = deviceConfiguration;
//    }

    //</editor-fold>

    //<editor-fold desc="Methods">

    private void setDeviceProperties() throws JposException{
        Assert.notNull(this.getPosDevice(), MessageFormat.format("{0} {1} instance not set.",this.getDeviceLogicalName(),this.getDeviceType()));

        ((Scale)this.getPosDevice()).setDataEventEnabled(true);
        this.bindListeners();
    }

    private void bindListeners(){
        Assert.notNull(this.getPosDevice(), MessageFormat.format("{0} {1} instance not set.",this.getDeviceLogicalName(),this.getDeviceType()));

        ((Scale)this.getPosDevice()).addDataListener(this);
        ((Scale)this.getPosDevice()).addErrorListener(this);
    }

    private void unbindListeners(){
        Assert.notNull(this.getPosDevice(), MessageFormat.format("{0} {1} instance not set.",this.getDeviceLogicalName(),this.getDeviceType()));

        ((Scale)this.getPosDevice()).removeDataListener(this);
        ((Scale)this.getPosDevice()).removeErrorListener(this);
    }

    private void clearData() throws JposException{
        Assert.notNull(this.getPosDevice(), MessageFormat.format("{0} {1} instance not set.",this.getDeviceLogicalName(),this.getDeviceType()));

        ((Scale)this.getPosDevice()).clearInput();
        ((Scale)this.getPosDevice()).setDataEventEnabled(true);

        log.info(MessageFormat.format("ScaleServiceManager->clearData: clearing data for {0} {1}",this.getDeviceLogicalName(),this.getDeviceType()));
    }

    //</editor-fold>

    //<editor-fold desc="Overridden Methods">


    @Override
    public void open() throws JposException {
        super.open();
        //((Scale)this.getPosDevice()).setAsyncMode(deviceConfiguration.isScaleAsync());
        if(executionHelper.isTest()){
            ((SimulatedTestingScale)this.getPosDevice()).setAsyncMode(deviceConfiguration.isScaleAsync());
        }else{
            ((Scale)this.getPosDevice()).setAsyncMode(deviceConfiguration.isScaleAsync());
        }
    }

    @Override
    public void enableDevice() throws JposException {
        super.enableDevice();
        this.setDeviceProperties();
    }

    @Override
    public void disableDevice() throws JposException {
        this.unbindListeners();
        super.disableDevice();
    }

    @Override
    public void sendData(Hardware hardwareObject, Hardware resultObject) throws Exception {
        com.vme.intellistore.hardware.commons.dtos.devices.Scale scaleObject = (com.vme.intellistore.hardware.commons.dtos.devices.Scale) hardwareObject;

        switch (scaleObject.getFunction().getFunctionType()){
            case DisplayText:
                ((Scale)this.getPosDevice()).displayText(scaleObject.getFunction().getFunctionParameter().toString());
                break;
            case ReadWeight:
                int[] result = new int[1];
                ((Scale)this.getPosDevice()).readWeight(result,0);
                int unit = ((Scale)this.getPosDevice()).getWeightUnit();
                ((com.vme.intellistore.hardware.commons.dtos.devices.Scale)resultObject).setUnits(WeightMeasurementUnits.fromInt(unit));
                ((com.vme.intellistore.hardware.commons.dtos.devices.Scale)resultObject).setWeight(result[0]);
                break;
            case ResetWeight:
                ((Scale)this.getPosDevice()).zeroScale();
                break;
            default:
                break;
        }
    }

    //</editor-fold>

    //<editor-fold desc="Listeners Overridden Methods">

    @Override
    public void dataOccurred(DataEvent e) {
        try{
            int weight = e.getStatus();
            log.info(MessageFormat.format("ScaleServiceManager->dataOccurred: Device with name {0} of type {1} received data.",this.getDeviceLogicalName(),this.getDeviceType()));

            com.vme.intellistore.hardware.commons.dtos.devices.Scale scaleObject = new com.vme.intellistore.hardware.commons.dtos.devices.Scale(HardwareActionType.ReceivedData);
            scaleObject.setAsync(((Scale)this.getPosDevice()).getAsyncMode());
            scaleObject.setUnits(WeightMeasurementUnits.fromInt(((Scale)this.getPosDevice()).getWeightUnit()));
            scaleObject.setWeight(weight);
            scaleObject.setActionResult(true);
            scaleObject.setObjectStatus(this.getDeviceStatus());

            service.sendDeviceData(scaleObject);

            this.clearData();
        }catch (Exception ex){
            log.error("Error occurred on ScaleServiceManager->dataOccurred",ex);
        }
    }

    @Override
    public void errorOccurred(ErrorEvent e) {
        log.error("An error occurred on Scale device instance",e);
    }

    //</editor-fold>

    public void setScaleWeight(Double weightValue) throws Exception{
        if(this.getPosDevice() instanceof SimulatedTestingScale){
            ((SimulatedTestingScale)this.getPosDevice()).triggerScaleWeightUpdate(weightValue);
        }else{
            throw new Exception("setScaleWeight method is not available outside of testing execution context");
        }
    }
}
