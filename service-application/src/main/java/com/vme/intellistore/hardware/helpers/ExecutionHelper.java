package com.vme.intellistore.hardware.helpers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

@Service
public class ExecutionHelper {
    @Autowired
    Environment environment;

    public ExecutionHelper() {
    }

    public boolean isTest(){
        String[] profiles = this.environment.getActiveProfiles();
        boolean containsProfile = false;

        for(String current : profiles){
            if(current.equals("TEST")){
                containsProfile = true;
                break;
            }
        }

        return containsProfile;
    }
}
