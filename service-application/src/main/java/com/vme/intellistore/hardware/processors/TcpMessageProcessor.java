package com.vme.intellistore.hardware.processors;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.vme.intellistore.hardware.commons.dtos.devices.*;
import com.vme.intellistore.hardware.commons.dtos.requests.*;
import com.vme.intellistore.hardware.commons.dtos.responses.ConfigurationUpdateResponse;
import com.vme.intellistore.hardware.commons.dtos.responses.HardwareOperationResponse;
import com.vme.intellistore.hardware.commons.dtos.responses.QueryResponse;
import com.vme.intellistore.hardware.services.PosHardwareService;
import jdk.nashorn.internal.objects.annotations.Setter;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.data.MapEntry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Scope;
import org.springframework.integration.annotation.*;
import org.springframework.integration.json.JsonToObjectTransformer;
import org.springframework.integration.json.ObjectToJsonTransformer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * The TcpMessageProcessor class handles all TCP message handling procedures to/from the service
 */
@MessageEndpoint
@Slf4j
public class TcpMessageProcessor {

    UUID currentRequestId;

    @Autowired
    PosHardwareService service;

    @Transformer(inputChannel = "inboundTcp",outputChannel = "inboundJson")
    public String convertFromByte(byte[] bytes){
        log.info("Received the following message: " + new String(bytes));
        return new String(bytes);
    }

    @Router(inputChannel = "inboundJson")
    public String routeRequest(String json) throws IOException{
        ObjectMapper mapper = new ObjectMapper();
        JsonNode root = mapper.readTree(json);
        RequestType type = RequestType.valueOf(root.get("Type").asText());
        currentRequestId = UUID.fromString(root.get("Guid").asText());

        switch (type){
            case Query:
                return "inboundJsonQueryRequest";
            case HardwareOperation:
                return "jsonHardwarePayloadFilter";
            case ConfigurationUpdate:
                return "inboundJsonConfigurationUpdateRequest";
            default:
                return "inboundJsonDefaultRequest";
        }
    }

    //<editor-fold desc="Query Request Routing">

    @Transformer(inputChannel = "inboundJsonQueryRequest", outputChannel = "jsonToDevicesQuery")
    public String routeQueryRequest(String json) throws IOException{

        ObjectMapper mapper = new ObjectMapper();
        JsonNode root = mapper.readTree(json);
        JsonNode payload = root.get("Payload");

        return payload.toString();
    }

    @Bean
    @Transformer(inputChannel = "jsonToDevicesQuery", outputChannel = "inboundServiceQuery")
    public JsonToObjectTransformer requestToDevicesQuery(){
        return new JsonToObjectTransformer(Query.class);
    }

    @ServiceActivator(inputChannel = "inboundServiceQuery", outputChannel = "publishToClient")
    public QueryResponse toServiceQuery(Query query) {
        return service.requestDeviceInfo(currentRequestId,query);
    }

    //</editor-fold>

    //<editor-fold desc="Hardware Request Routing">

    @Transformer(inputChannel = "jsonHardwarePayloadFilter", outputChannel = "inboundJsonHardwareRequest")
    public String hardwarePayloadFilter(String fullJson) throws IOException{
        List<String> items = new ArrayList<>();
        ObjectMapper mapper = new ObjectMapper();
        JsonNode root = mapper.readTree(fullJson);
        //HardwareDeviceType type = HardwareDeviceType.valueOf(root.get("ObjectType").asText());
        JsonNode payload = root.get("Payload");

        return payload.toString();
    }

    @Router(inputChannel = "inboundJsonHardwareRequest")
    public String routeHardwareRequest(String json) throws IOException{
        List<String> routes = new ArrayList<>();

        ObjectMapper mapper = new ObjectMapper();

        JsonNode root = mapper.readTree(json);
        HardwareDeviceType type = HardwareDeviceType.valueOf(root.get("ObjectType").asText());

        switch (type) {
            case CashDrawer:
                return "jsonToCashDrawer";
            case LineDisplay:
                return "jsonToLineDisplay";
            case MSR:
                return "jsonToMsr";
            case POSPrinter:
                return "jsonToPosPrinter";
            case Scale:
                return "jsonToScale";
            case Scanner:
                return "jsonToScanner";
            default:
                return "jsonToDefault";
        }
    }

    @Bean
    @Transformer(inputChannel = "jsonToDefault", outputChannel = "inboundService")
    public JsonToObjectTransformer requestJsonToHardware(){
        return new JsonToObjectTransformer(Hardware.class);
    }

    @Bean
    @Transformer(inputChannel = "jsonToCashDrawer", outputChannel = "inboundService")
    public JsonToObjectTransformer requestJsonToCashDrawer(){
        return new JsonToObjectTransformer(CashDrawer.class);
    }

    @Bean
    @Transformer(inputChannel = "jsonToLineDisplay", outputChannel = "inboundService")
    public JsonToObjectTransformer requestJsonToLineDisplay(){
        return new JsonToObjectTransformer(LineDisplay.class);
    }

    @Bean
    @Transformer(inputChannel = "jsonToMsr", outputChannel = "inboundService")
    public JsonToObjectTransformer requestJsonToMsr(){
        return new JsonToObjectTransformer(Msr.class);
    }

    @Bean
    @Transformer(inputChannel = "jsonToPosPrinter", outputChannel = "inboundService")
    public JsonToObjectTransformer requestJsonToPosPrinter(){
        return new JsonToObjectTransformer(PosPrinter.class);
    }

    @Bean
    @Transformer(inputChannel = "jsonToScale", outputChannel = "inboundService")
    public JsonToObjectTransformer requestJsonToScale(){
        return new JsonToObjectTransformer(Scale.class);
    }

    @Bean
    @Transformer(inputChannel = "jsonToScanner", outputChannel = "inboundService")
    public JsonToObjectTransformer requestJsonToScanner(){
        return new JsonToObjectTransformer(Scanner.class);
    }

    @ServiceActivator(inputChannel = "inboundService",outputChannel = "publishToClient")
    public HardwareOperationResponse toService(Object object){

        return service.requestDeviceAction(currentRequestId,object);
    }

    //</editor-fold>

    //<editor-fold desc="Hardware Config Update Request">

    @Transformer(inputChannel = "inboundJsonConfigurationUpdateRequest", outputChannel = "jsonToConfigurationRequest")
    public String routeConfigUpdateRequest(String json) throws IOException{

        ObjectMapper mapper = new ObjectMapper();
        JsonNode root = mapper.readTree(json);
        JsonNode payload = root.get("Payload");

        return payload.toString();
    }

    @Bean
    @Transformer(inputChannel = "jsonToConfigurationRequest", outputChannel = "inboundConfigurationUpdate")
    public JsonToObjectTransformer requestToConfigUpdate(){
        return new JsonToObjectTransformer(DeviceConfigurationUpdate.class);
    }

    @ServiceActivator(inputChannel = "inboundConfigurationUpdate", outputChannel = "publishToClient")
    public ConfigurationUpdateResponse toServiceConfigUpdate(DeviceConfigurationUpdate update){
        return service.updateSelectedDeviceConfiguration(currentRequestId,update);
    }

    //</editor-fold>

    //This is specifically used to output device data event to the client
    @Publisher(channel = "publishToClient")
    public HardwareOperationResponse publishDeviceObject(HardwareOperationResponse device){
        return device;
    }

    @Bean
    @Transformer(inputChannel = "publishToClient",outputChannel = "toClient")
    public ObjectToJsonTransformer publishObjectToJsonTransformer(){
        return new ObjectToJsonTransformer();
    }
}
