package com.vme.intellistore.hardware.managers;

import com.vme.intellistore.hardware.commons.dtos.devices.CashDrawer;
import com.vme.intellistore.hardware.commons.dtos.devices.HardwareDeviceType;
import com.vme.intellistore.hardware.configuration.DeviceConfiguration;
import com.vme.intellistore.hardware.helpers.ExecutionHelper;
import com.vme.intellistore.hardware.services.PosHardwareService;
import jpos.JposException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.NotImplementedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import javax.annotation.PreDestroy;
import java.text.MessageFormat;
import java.util.HashMap;

/**
 * Created by Zazza on 22/05/2017.
 */
@Service
@Slf4j
public class ServiceManagerHelper {

    //<editor-fold desc="Properties">

    @Autowired
    private PosHardwareService service;

    @Autowired
    private DeviceConfiguration deviceConfiguration;

    @Autowired
    private ApplicationContext applicationContext;

    private HashMap<String,HardwareServiceManager> serviceManagers;

    //</editor-fold>

    //<editor-fold desc="Methods">

    @PreDestroy
    public void cleanUp(){
        this.releaseAllDeviceInstances();
        this.closeAllDeviceInstances();
    }

    public HardwareServiceManager getInstance(HardwareDeviceType type, String deviceLogicalName){
        HardwareServiceManager instance = null;

        if(serviceManagers == null){
            serviceManagers = new HashMap<>();
        }else{
            instance = serviceManagers.get(deviceLogicalName);
        }

        if(instance == null) {

            switch (type) {
                case CashDrawer:
                    instance = applicationContext.getBean(CashDrawerServiceManager.class);
                    break;
                case LineDisplay:
                    instance = applicationContext.getBean(LineDisplayServiceManager.class);
                    break;
                case MSR:
                    instance = applicationContext.getBean(MsrServiceManager.class);
                    break;
                case POSPrinter:
                    instance = applicationContext.getBean(PosPrinterServiceManager.class);
                    break;
                case Scale:
                    instance = applicationContext.getBean(ScaleServiceManager.class);
                    break;
                case Scanner:
                    instance = applicationContext.getBean(ScannerServiceManager.class);
                    break;
                default:
                    throw new NotImplementedException();
            }

            instance.setDeviceLogicalName(deviceLogicalName);
            instance.setService(this.service);
            serviceManagers.put(deviceLogicalName,instance);
        }

        return instance;
    }

    public void closeAllDeviceInstances(){
        for(HardwareServiceManager item : serviceManagers.values()){
            try {
                item.close();
            }catch (JposException e){
                log.error(MessageFormat.format("An exception occurred during CloseAll for device {0} type {1}",item.getDeviceLogicalName(),item.getDeviceType()),e);
            }
        }
    }

    public void releaseAllDeviceInstances(){
        for(HardwareServiceManager item : serviceManagers.values()){
            try {
                item.release();
            }catch (JposException e){
                log.error(MessageFormat.format("An exception occurred during ReleaseAll for device {0} type {1}",item.getDeviceLogicalName(),item.getDeviceType()),e);
            }
        }
    }

    //</editor-fold>

}
