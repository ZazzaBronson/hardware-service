package com.vme.intellistore.hardware.helpers;

import com.vme.intellistore.hardware.managers.ServiceManagerHelper;

public interface SpringContextBridgeServices {
    ServiceManagerHelper getServiceManagerHelper();
}
