package com.vme.intellistore.hardware.services;

import com.vme.intellistore.hardware.helpers.SpringContextBridge;
import com.vme.intellistore.hardware.managers.ServiceManagerHelper;

public class TestingService {

    public static ServiceManagerHelper getServiceManagerHelper(){
        ServiceManagerHelper serviceManagerHelper = SpringContextBridge.services().getServiceManagerHelper();
        return serviceManagerHelper;
    }
}
