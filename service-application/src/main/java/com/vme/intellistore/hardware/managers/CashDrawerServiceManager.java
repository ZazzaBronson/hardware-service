package com.vme.intellistore.hardware.managers;

import com.vme.intellistore.hardware.commons.dtos.devices.CashDrawerStatus;
import com.vme.intellistore.hardware.commons.dtos.devices.Hardware;
import com.vme.intellistore.hardware.commons.dtos.devices.HardwareActionType;
import com.vme.intellistore.javapos.device.simulator.testing.beans.SimulatedTestingCashDrawer;
import jpos.CashDrawer;
import jpos.JposException;
import jpos.events.StatusUpdateEvent;
import jpos.events.StatusUpdateListener;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.NotImplementedException;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

import javax.annotation.PostConstruct;
import java.text.MessageFormat;

/**
 * Created by Mirko on 24-Jan-17.
 */
@Component
@Slf4j
@Scope("prototype")
public class CashDrawerServiceManager extends HardwareServiceManager implements StatusUpdateListener {

    //<editor-fold desc="Constants">

    private static final int CASH_SUE_DRAWERCLOSED = 0;
    private static final int CASH_SUE_DRAWEROPEN= 1;

    //</editor-fold>

    //<editor-fold desc="Constructors">

    protected CashDrawerServiceManager(){
        this.setDeviceType("Cash Drawer");
    }

    @PostConstruct
    private void init(){
        this.setPosDevice(executionHelper.isTest() ? new SimulatedTestingCashDrawer() : new CashDrawer());
    }

//    protected CashDrawerServiceManager() {
//        this.setDeviceType("Cash Drawer");
//        this.setPosDevice(new CashDrawer());
//    }
//
//    protected CashDrawerServiceManager(String logicalName){
//        this.setDeviceType("Cash Drawer");
//        this.setPosDevice(new CashDrawer());
//        this.setDeviceLogicalName(logicalName);
//    }

    //</editor-fold>

    //<editor-fold desc="Overridden Methods">

    @Override
    public void enableDevice() throws JposException {
        super.enableDevice();
        this.bindListeners();
    }

    @Override
    public void disableDevice() throws JposException {
        this.unbindListeners();
        super.disableDevice();
    }

    @Override
    public void sendData(Hardware hardwareObject, Hardware resultObject) throws Exception {
        throw new NotImplementedException("CashDrawer does not expose a sendData method");
    }

    //</editor-fold>

    //<editor-fold desc="Methods">

    private void bindListeners(){
        Assert.notNull(this.getPosDevice(), MessageFormat.format("{0} {1} instance not set.",this.getDeviceLogicalName(),this.getDeviceType()));

        ((CashDrawer)this.getPosDevice()).addStatusUpdateListener(this);
    }

    private void unbindListeners(){
        Assert.notNull(this.getPosDevice(), MessageFormat.format("{0} {1} instance not set.",this.getDeviceLogicalName(),this.getDeviceType()));

        ((CashDrawer)this.getPosDevice()).removeStatusUpdateListener(this);
    }

    public Boolean openDrawer() throws JposException,Exception{
        Assert.notNull(this.getPosDevice(), MessageFormat.format("{0} {1} instance not set.",this.getDeviceLogicalName(),this.getDeviceType()));

        if(!this.canProcess()){
            throw new Exception("openDrawer - CashDrawer is not initialised");
        }

        Boolean result = false;

        if(((CashDrawer)this.getPosDevice()).getDrawerOpened()){
            log.info("Cash drawer is already open");
        }else{
            log.info("Opening cash drawer");
            ((CashDrawer)this.getPosDevice()).openDrawer();
            result = true;
            log.info("Opened the cash drawer");
        }

        return result;
    }

    //</editor-fold>

    //<editor-fold desc="Listener Method Overrides">

    @Override
    public void statusUpdateOccurred(StatusUpdateEvent e) {
        boolean isOpen = e.getStatus() == CASH_SUE_DRAWEROPEN ? true : false;

        log.info(MessageFormat.format("CashDrawer status update IsOpen: {0}",isOpen));

        try{
            CashDrawerStatus status = isOpen == true ? CashDrawerStatus.Open : CashDrawerStatus.Closed;

            com.vme.intellistore.hardware.commons.dtos.devices.CashDrawer cashDrawerObject = new com.vme.intellistore.hardware.commons.dtos.devices.CashDrawer(HardwareActionType.StatusUpdated);
            cashDrawerObject.setActionResult(true);
            cashDrawerObject.setStatus(status);
            cashDrawerObject.setObjectStatus(this.getDeviceStatus());

            service.sendDeviceData(cashDrawerObject);
        }catch (Exception ex){
            log.error("Exception occurred on CashDrawer instance statusUpdateOccurred",ex);
        }
    }

    //</editor-fold>

}
