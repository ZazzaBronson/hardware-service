package com.vme.intellistore.hardware.configuration;

import jpos.config.JposEntryRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/***
 * Configuration class which will handle the exposure of any configured properties and system-wide available resources
 */
@Configuration
@ComponentScan("com.vme.intellistore.hardware")
@EnableAutoConfiguration
//@PropertySource(value = "classpath:application.properties")
public class BaseConfiguration {

    @Autowired
    JposEntryRegistry entryRegistry;
}
