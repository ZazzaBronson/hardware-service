package com.vme.intellistore.hardware.configuration;

import lombok.Getter;
import lombok.Setter;
import org.aopalliance.aop.Advice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.integration.annotation.Gateway;
import org.springframework.integration.annotation.IntegrationComponentScan;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.channel.PublishSubscribeChannel;
import org.springframework.integration.channel.QueueChannel;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.handler.DelayHandler;
import org.springframework.integration.handler.advice.ErrorMessageSendingRecoverer;
import org.springframework.integration.handler.advice.RequestHandlerRetryAdvice;
import org.springframework.integration.ip.tcp.TcpInboundGateway;
import org.springframework.integration.ip.tcp.TcpSendingMessageHandler;
import org.springframework.integration.ip.tcp.connection.*;
import org.springframework.integration.ip.tcp.serializer.ByteArrayRawSerializer;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHandler;
import org.springframework.retry.backoff.ExponentialBackOffPolicy;
import org.springframework.retry.policy.SimpleRetryPolicy;
import org.springframework.retry.support.RetryTemplate;

/**
 * The ServerConfiguration class contains all configured components for the TCP server
 */
@EnableIntegration
@IntegrationComponentScan
@Configuration
@ConfigurationProperties(prefix = "tcp.service")
public class ServerConfiguration {

    @Getter @Setter
    private int serverPort;

    @Getter @Setter
    private String clientUri;

    @Getter @Setter
    private int clientPort;

//    @Getter
//    private String connectionId;

    @Autowired
    BaseConfiguration configuration;

    /***
     * Bean which will produce a ServerConnection based on the configured port
     * @return
     */
    @Bean
    public AbstractServerConnectionFactory serverConnectionFactory(){
        TcpNetServerConnectionFactory connectionFactory = new TcpNetServerConnectionFactory(this.getServerPort());
        ByteArrayRawSerializer serializer = new ByteArrayRawSerializer();
        serializer.setMaxMessageSize(102400);
//        connectionFactory.setDeserializer(new ByteArrayRawSerializer());
        connectionFactory.setDeserializer(serializer);
        return connectionFactory;
    }

    /***
     * Bean which will produce a ClientConnection based on the configured port
     * @return
     */
    @Bean
    public AbstractClientConnectionFactory clientConnectionFactory(){
        TcpNetClientConnectionFactory connectionFactory = new TcpNetClientConnectionFactory(this.getClientUri(),this.getClientPort());
        connectionFactory.setSingleUse(true);
        return connectionFactory;
    }

    @Bean
    public MessageChannel inboundTcp(){
        return new DirectChannel();
    }

    @Bean
    public MessageChannel outboundTcp(){ return new DirectChannel(); }

    @Bean
    public TcpInboundGateway tcpInboundGateway(AbstractServerConnectionFactory connectionFactory){
        TcpInboundGateway gateway = new TcpInboundGateway();
        gateway.setConnectionFactory(connectionFactory);
        gateway.setRequestChannel(this.inboundTcp());
        return gateway;
    }

    @Bean
    @ServiceActivator(inputChannel = "toClient")
    public TcpSendingMessageHandler tcpOutboundMessageHandler(AbstractClientConnectionFactory connectionFactory){
        TcpSendingMessageHandler messageHandler = new TcpSendingMessageHandler();
        messageHandler.setConnectionFactory(connectionFactory);
        messageHandler.setClientMode(false);

        return messageHandler;
    }
}
