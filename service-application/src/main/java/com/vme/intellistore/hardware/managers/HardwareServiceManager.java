package com.vme.intellistore.hardware.managers;

import com.vme.intellistore.hardware.commons.dtos.devices.Hardware;
import com.vme.intellistore.hardware.commons.dtos.devices.HardwareDeviceStatus;
import com.vme.intellistore.hardware.helpers.ExecutionHelper;
import com.vme.intellistore.hardware.services.PosHardwareService;
import jpos.*;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.text.MessageFormat;

/**
 * Created by Mirko on 18-Jan-17.
 */
@Component
@Slf4j
@Scope("prototype")
public abstract class HardwareServiceManager {

    @Autowired
    protected ExecutionHelper executionHelper;

    @Getter @Setter
    protected PosHardwareService service;

    //<editor-fold desc="Properties">

    @Getter @Setter
    private String deviceType;

    @Getter @Setter
    private String deviceLogicalName;

    @Getter @Setter
    private BaseControl posDevice;

    @Getter @Setter
    private int claimTimespan;

    //</editor-fold>

    //<editor-fold desc="Methods">

    public void claim() throws JposException{
        BaseJposControl control = (BaseJposControl) this.posDevice;

        Assert.notNull(control,MessageFormat.format("{0} {1} instance not set",this.deviceLogicalName,this.deviceType));

        if(control.getClaimed()){
            return;
        }

        control.claim(this.claimTimespan);

        log.info(MessageFormat.format("{0} {1} claimed",this.deviceLogicalName,this.deviceType));
    }

    public void close() throws JposException{
        Assert.notNull(this.getPosDevice(),"PosDevice not set");
        this.getPosDevice().close();
        log.info(MessageFormat.format("{0} {1} closed.",this.getDeviceLogicalName(),this.getDeviceType()));
    }

    public void open() throws JposException{
        Assert.notNull(this.getPosDevice(),"PosDevice not set");

        this.getPosDevice().open(this.getDeviceLogicalName());

        log.info(MessageFormat.format("{0} {1} opened.",this.getDeviceLogicalName(),this.getDeviceType()));
    }

    public void enableDevice() throws JposException{
        BaseJposControl control = (BaseJposControl) this.posDevice;

        Assert.notNull(control,MessageFormat.format("{0} {1} instance not set",this.deviceLogicalName,this.deviceType));

        if(control.getDeviceEnabled()){
            log.info(MessageFormat.format("{0} {1} already enabled, exiting.",this.deviceLogicalName,this.deviceType));
            return;
        }

        control.setDeviceEnabled(true);

        log.info(MessageFormat.format("{0} {1} enabled.",this.deviceLogicalName,this.deviceType));
    }

    public void disableDevice() throws JposException{
        BaseJposControl control = (BaseJposControl) this.posDevice;

        Assert.notNull(control,MessageFormat.format("{0} {1} instance not set",this.deviceLogicalName,this.deviceType));

        control.setDeviceEnabled(false);

        log.info(MessageFormat.format("{0} {1} disabled.",this.deviceLogicalName,this.deviceType));
    }

    public void release() throws JposException{
        BaseJposControl control = (BaseJposControl) this.posDevice;

        Assert.notNull(control,MessageFormat.format("{0} {1} instance not set",this.deviceLogicalName,this.deviceType));

        if(control.getDeviceEnabled()){
            this.disableDevice();
        }

        control.release();

        log.info(MessageFormat.format("{0} {1} released.",this.deviceLogicalName,this.deviceType));
    }

    public void initialise(Hardware hardwareObject) throws JposException{
        Assert.notNull(this.deviceType,"DeviceType is not set");

        this.open();
        this.claim();
        this.enableDevice();
    }

    public boolean verify() {
        Assert.notNull(this.deviceType,"DeviceType is not set");
        boolean verified = false;

        if(this.getDeviceStatus() == HardwareDeviceStatus.Closed) {
            try {
                this.open();
            } catch (JposException e) {
                log.error(MessageFormat.format("Exception occurred whilst verifying (OPEN) device {0}", this.getDeviceLogicalName()), e);
            } finally {
                try {
                    this.close();
                    verified = true;
                } catch (JposException e) {
                    log.error(MessageFormat.format("Exception occurred whilst verifying (CLOSE) device {0}", this.getDeviceLogicalName()), e);
                    verified = false;
                }
            }
        }else if(this.getDeviceStatus() == HardwareDeviceStatus.Idle || this.getDeviceStatus() == HardwareDeviceStatus.Busy){
            verified = true;
        }


        return verified;
    }

    public abstract void sendData(Hardware hardwareObject, Hardware resultObject) throws Exception;

    public boolean canProcess(){
        Assert.notNull(this.getPosDevice(), MessageFormat.format("{0} {1} instance not set.",this.getDeviceLogicalName(),this.getDeviceType()));
        boolean result = false;

        try {
            if (this.getPosDevice().getState() == JposConst.JPOS_S_IDLE && this.getPosDevice().getClaimed() && this.getPosDevice().getDeviceEnabled()) {
                result = true;
            }
        }catch (JposException e){
            log.error(MessageFormat.format("Error occurred whilst evaluating canProcess for {0} {1}",this.getDeviceLogicalName(),this.getDeviceType()),e);
        }

        return result;
    }

    public boolean isClaimed(){
        Assert.notNull(this.getPosDevice(), MessageFormat.format("{0} {1} instance not set.",this.getDeviceLogicalName(),this.getDeviceType()));
        boolean result = false;

        try {
            result = this.getPosDevice().getClaimed();
        }catch (JposException e){
            log.error(MessageFormat.format("Error occurred whilst evaluating isClaimed for {0} {1}",this.getDeviceLogicalName(),this.getDeviceType()),e);
        }

        return result;
    }

    public boolean isEnabled(){
        Assert.notNull(this.getPosDevice(), MessageFormat.format("{0} {1} instance not set.",this.getDeviceLogicalName(),this.getDeviceType()));
        boolean result = false;

        try {
            result = this.getPosDevice().getDeviceEnabled();
        }catch (JposException e){
            log.error(MessageFormat.format("Error occurred whilst evaluating isEnabled for {0} {1}",this.getDeviceLogicalName(),this.getDeviceType()),e);
        }

        return result;
    }

    public HardwareDeviceStatus getDeviceStatus(){
        Assert.notNull(this.getPosDevice(),"PosDevice not set");
        int status = this.getPosDevice().getState();
        log.info(MessageFormat.format("{0} {1} status: {2}.",this.getDeviceLogicalName(),this.getDeviceType(),status));

        return HardwareDeviceStatus.fromInt(status);//HardwareDeviceStatus.values()[status];
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        HardwareServiceManager that = (HardwareServiceManager) o;

        if (!deviceType.equals(that.deviceType)) return false;
        return deviceLogicalName.equals(that.deviceLogicalName);
    }

    @Override
    public int hashCode() {
        int result = deviceType.hashCode();
        result = 31 * result + deviceLogicalName.hashCode();
        return result;
    }

    //</editor-fold>
}
