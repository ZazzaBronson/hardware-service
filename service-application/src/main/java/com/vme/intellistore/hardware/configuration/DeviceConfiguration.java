package com.vme.intellistore.hardware.configuration;

import jpos.config.JposEntry;
import jpos.config.JposEntryRegistry;
import jpos.loader.JposServiceLoader;
import jpos.profile.ProfileRegistry;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * The DeviceConfiguration class is tasked with handling Jpos device configuration and JposRegistry initialisation
 */
@Configuration
@ConfigurationProperties(prefix = "devices")
@Getter
@Setter
@Service
public class DeviceConfiguration {

    private String nameCashDrawer;
    private String nameLineDisplay;
    private String nameMsr;
    private String namePosPrinter;
    private String nameScale;
    private String nameScanner;
    private boolean scaleAsync;

    @Bean(name = "deviceConfigurationRegistration")
    public JposEntryRegistry getRegistry(){
        JposEntryRegistry registry = null;

        JposServiceLoader.getManager().reloadEntryRegistry();
        registry = JposServiceLoader.getManager().getEntryRegistry();

        return registry;
    }
}
