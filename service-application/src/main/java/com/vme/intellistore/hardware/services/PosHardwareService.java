package com.vme.intellistore.hardware.services;

import com.vme.intellistore.hardware.commons.dtos.devices.*;
import com.vme.intellistore.hardware.commons.dtos.devices.Scanner;
import com.vme.intellistore.hardware.commons.dtos.requests.DeviceConfigurationUpdate;
import com.vme.intellistore.hardware.commons.dtos.requests.Query;
import com.vme.intellistore.hardware.commons.dtos.responses.ConfigurationUpdateResponse;
import com.vme.intellistore.hardware.commons.dtos.responses.HardwareOperationResponse;
import com.vme.intellistore.hardware.commons.dtos.responses.QueryResponse;
import com.vme.intellistore.hardware.configuration.DeviceConfiguration;
import com.vme.intellistore.hardware.managers.*;
import com.vme.intellistore.hardware.processors.TcpMessageProcessor;
import jpos.config.JposEntry;
import jpos.config.JposEntryRegistry;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.NotImplementedException;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Created by Mirko on 30-Jan-17.
 */
@Service
@Slf4j
public class PosHardwareService {

    //<editor-fold desc="Fields">

    private List<Map<String,Object>> rawEntryList = new ArrayList<>();

    private Map<HardwareDeviceType,List<HardwareDeviceInfo>> formattedEntryList = new HashMap<>();

    @Autowired
    JposEntryRegistry entryRegistry;

    @Autowired
    DeviceConfiguration deviceConfiguration;

    @Autowired
    ConfigurationService configurationService;

    @Autowired
    TcpMessageProcessor processor;

    @Autowired
    ServiceManagerHelper serviceManagerHelper;

    //</editor-fold>

    //<editor-fold desc="Methods">

    public void sendDeviceData(Hardware device){
        processor.publishDeviceObject(new HardwareOperationResponse(UUID.randomUUID(),device));
    }

    public HardwareOperationResponse requestDeviceAction(UUID requestId,Object device){
        return new HardwareOperationResponse(requestId,this.executeDeviceAction(device));
    }

    public QueryResponse requestDeviceInfo(UUID requestId, Query query){
        QueryResponse response = null;

        switch (query.getType()){
            case ListAllAvailableDevices:
                response = new QueryResponse(requestId,getAllAvailableDevices());
                break;
            case ListAllAvailableDevicesForType:
                response = new QueryResponse(requestId,getAvailableDevicesForType(HardwareDeviceType.valueOf(query.getParameter())));
                break;
            case ListDevice:
                response = new QueryResponse(requestId,getDevice(query.getParameter()));
                break;
            case GetConfiguredDeviceInfo:
                response = new QueryResponse(requestId,getConfiguredDevice(HardwareDeviceType.valueOf(query.getParameter())));
                break;
            default:
                response = null;
                break;
        }

//        this.delay(500);
        return response;
    }

    public ConfigurationUpdateResponse updateSelectedDeviceConfiguration(UUID requestId,DeviceConfigurationUpdate update){
        Boolean updated = false;
        try {
            configurationService.setSelectedDevice(update.getType(),update.getDeviceLogicalName());
            updated = true;
        }catch (Exception e){
            log.error("An error occurred whilst updating selected device configuration",e);
        }

//        this.delay(500);
        return new ConfigurationUpdateResponse(requestId, updated);
    }

    private String getDeviceLogicalName(HardwareDeviceType type){
        switch (type){
            case CashDrawer:
                return deviceConfiguration.getNameCashDrawer();
            case LineDisplay:
                return deviceConfiguration.getNameLineDisplay();
            case MSR:
                return deviceConfiguration.getNameMsr();
            case POSPrinter:
                return deviceConfiguration.getNamePosPrinter();
            case Scale:
                return deviceConfiguration.getNameScale();
            case Scanner:
                return deviceConfiguration.getNameScanner();
            default:
                throw new NotImplementedException();
        }
    }

    private Hardware getDeviceInstance(Object object){
        if(object instanceof CashDrawer){
            return (CashDrawer) object;
        }else if(object instanceof LineDisplay){
            return (LineDisplay) object;
        }else if(object instanceof Msr){
            return (Msr) object;
        }else if(object instanceof PosPrinter){
            return (PosPrinter) object;
        }else if(object instanceof Scale){
            return (Scale) object;
        }else if(object instanceof Scanner){
            return (Scanner) object;
        }else{
            return null;
        }
    }

    private Hardware executeDeviceAction(Object object){
        Hardware device = this.getDeviceInstance(object);
        Assert.notNull(device,"PosHardwareService->executeDeviceAction, Hardware object instance null");
        Assert.notNull(device.getObjectType(),"PosHardwareService->executeDeviceAction, Hardware object objectType property is null");
        Assert.notNull(device.getActionType(),"PosHardwareService->executeDeviceAction, Hardware object actionType property is null");

        Hardware responseObject = device;//this.getDeviceTypeInstancee(device);//new Hardware(object.getActionType());

        try {
            //If logicalName is available in the request object, set the manager to use that, otherwise use the one set in configuration
            if(device.getLogicalName() == null){
                device.setLogicalName("");
            }

            String deviceName = device.getLogicalName().isEmpty() ? this.getDeviceLogicalName(device.getObjectType()) : device.getLogicalName();

            HardwareServiceManager manager = serviceManagerHelper.getInstance(device.getObjectType(),deviceName);
            responseObject.setLogicalName(manager.getDeviceLogicalName());

            switch (device.getActionType()){
                case Claim:
                    manager.claim();
                    responseObject.setActionType(HardwareActionType.Claimed);
                    break;
                case Close:
                    manager.close();
                    responseObject.setActionType(HardwareActionType.Closed);
                    break;
                case Disable:
                    manager.disableDevice();
                    responseObject.setActionType(HardwareActionType.Disabled);
                    break;
                case Enable:
                    manager.enableDevice();
                    responseObject.setActionType(HardwareActionType.Enabled);
                    break;
                case Initialise:
                    manager.initialise(device);
                    responseObject.setActionType(HardwareActionType.Initialised);
                    break;
                case Open:
                    manager.open();
                    responseObject.setActionType(HardwareActionType.Opened);
                    break;
                case Release:
                    manager.release();
                    responseObject.setActionType(HardwareActionType.Released);
                    break;
                case SendData:
                    manager.sendData(device,responseObject);
                    responseObject.setActionType(HardwareActionType.SentData);
                    break;
                default:
                    throw new Exception("Invalid ActionType selected");
            }

            responseObject.setObjectStatus(manager.getDeviceStatus());
            responseObject.setActionResult(true);
            responseObject.setActionException(null);
        }catch (Exception e){
            log.error("PosHardwareService->executeDeviceAction, Exception occurred for hardware request: [" + device.toString() + "]",e);
            responseObject.setActionResult(false);
            responseObject.setActionException(e.getMessage());
        }

        return responseObject;
    }

    private Map<HardwareDeviceType,List<HardwareDeviceInfo>> getAllAvailableDevices(){
        this.formattedEntryList = new HashMap<>();
        this.fillFormattedEntryList();

        return this.formattedEntryList;
    }

    private Map<HardwareDeviceType,List<HardwareDeviceInfo>> getAvailableDevicesForType(HardwareDeviceType type){
        if(this.formattedEntryList.isEmpty()){
            this.fillFormattedEntryList();
        }

        List<HardwareDeviceInfo> devices = this.formattedEntryList.get(type);
        Map<HardwareDeviceType,List<HardwareDeviceInfo>> result = new HashMap<>();
        result.put(type,devices);

        return result;
    }

    private Map<HardwareDeviceType,List<HardwareDeviceInfo>> getDevice(String logicalName){
        if(this.formattedEntryList.isEmpty()){
            this.fillFormattedEntryList();
        }

        Map<String,Object> deviceProps = this.rawEntryList.stream().filter(i -> i.get("logicalName").equals(logicalName)).findFirst().get();
        String name = deviceProps.get("logicalName").toString();
        HardwareDeviceType type = HardwareDeviceType.valueOf(deviceProps.get("deviceCategory").toString());
        HardwareServiceManager manager = serviceManagerHelper.getInstance(type,name); //this.getNewServiceManager(type,name);
        HardwareDeviceInfo device = new HardwareDeviceInfo(type,name,this.isConfigured(name),manager.getDeviceStatus(),manager.isClaimed(),manager.isEnabled(),manager.verify(),deviceProps);
        List<HardwareDeviceInfo> devices = new ArrayList<>();
        devices.add(device);

        Map<HardwareDeviceType,List<HardwareDeviceInfo>> result = new HashMap<>();
        result.put(device.getDeviceType(),devices);

        return result;
    }

    private Map<HardwareDeviceType,List<HardwareDeviceInfo>> getConfiguredDevice(HardwareDeviceType type){
        String deviceLogicalName = "";

        switch (type){
            case MSR:
                deviceLogicalName = this.deviceConfiguration.getNameMsr();
                break;
            case CashDrawer:
                deviceLogicalName = this.deviceConfiguration.getNameCashDrawer();
                break;
            case LineDisplay:
                deviceLogicalName = this.deviceConfiguration.getNameLineDisplay();
                break;
            case POSPrinter:
                deviceLogicalName = this.deviceConfiguration.getNamePosPrinter();
                break;
            case Scale:
                deviceLogicalName = this.deviceConfiguration.getNameScale();
                break;
            case Scanner:
                deviceLogicalName = this.deviceConfiguration.getNameScanner();
                break;
            default:
                break;
        }

        return this.getDevice(deviceLogicalName);
    }

    private void fillRawEntryList(){
        Enumeration<JposEntry> entries = entryRegistry.getEntries();

        while(entries.hasMoreElements()){
            JposEntry current = entries.nextElement();

            Map<String,Object> properties = new HashMap<>();
            Iterator<JposEntry.Prop> iterator = current.getProps();

            while(iterator.hasNext()){
                JposEntry.Prop p = iterator.next();
                properties.put(p.getName(),p.getValue());
            }

            rawEntryList.add(properties);
        }
    }

    private void fillFormattedEntryList(){
        if(this.rawEntryList.isEmpty()){
            fillRawEntryList();
        }

        List<String> names = this.getConfiguredDevicesName();

        for(HardwareDeviceType type : HardwareDeviceType.values()){
            Predicate<Map<String,Object>> predicate = p -> p.get("deviceCategory").equals(type.toString());
            List<Map<String,Object>> filtered = this.rawEntryList.stream().filter(predicate).collect(Collectors.toList());

            List<HardwareDeviceInfo> devices = new ArrayList<>();

            for (Map<String,Object> item : filtered) {
                String name = item.get("logicalName").toString();
                HardwareServiceManager manager = serviceManagerHelper.getInstance(type,name); //this.getNewServiceManager(type,name);
                devices.add(new HardwareDeviceInfo(type,name,this.isConfigured(name),manager.getDeviceStatus(),manager.isClaimed(),manager.isEnabled(),manager.verify(),item));
            }

            this.formattedEntryList.put(type,devices);
        }
    }

    private List<String> getConfiguredDevicesName(){
        List<String> names = new ArrayList<>();

        names.add(this.deviceConfiguration.getNameCashDrawer());
        names.add(this.deviceConfiguration.getNameLineDisplay());
        names.add(this.deviceConfiguration.getNameMsr());
        names.add(this.deviceConfiguration.getNamePosPrinter());
        names.add(this.deviceConfiguration.getNameScale());
        names.add(this.deviceConfiguration.getNameScanner());

        return names;
    }

    @NotNull
    private Boolean isConfigured(String deviceName){
        return this.getConfiguredDevicesName().contains(deviceName);
    }

    //</editor-fold>
}
