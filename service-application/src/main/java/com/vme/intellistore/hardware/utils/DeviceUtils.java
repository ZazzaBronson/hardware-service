package com.vme.intellistore.hardware.utils;

import java.util.ArrayList;

/**
 * Created by Mirko on 26-Jan-17.
 */
public class DeviceUtils {
    public static ArrayList<Integer> getCharacterSets(String list){
        ArrayList<Integer> sets = new ArrayList<Integer>();
        String[] items = list.split(",");

        for (String item : items) {
            sets.add(Integer.parseInt(item));
        }

        return sets;
    }
}
