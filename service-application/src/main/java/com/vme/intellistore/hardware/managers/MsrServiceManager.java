package com.vme.intellistore.hardware.managers;

import com.vme.intellistore.hardware.commons.dtos.devices.Hardware;
import com.vme.intellistore.hardware.commons.dtos.devices.HardwareActionType;
import com.vme.intellistore.hardware.commons.dtos.devices.Msr;
import com.vme.intellistore.javapos.device.simulator.testing.beans.SimulatedTestingMSR;
import jpos.JposException;
import jpos.MSR;
import jpos.events.*;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.NotImplementedException;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.annotation.PostConstruct;
import java.text.MessageFormat;

/**
 * Created by Mirko on 25-Jan-17.
 */
@Component
@Slf4j
@Scope("prototype")
public class MsrServiceManager extends HardwareServiceManager implements DataListener, StatusUpdateListener, ErrorListener {

    //<editor-fold desc="Constructors">

    protected MsrServiceManager(){
        this.setDeviceType("Msr");
    }

    @PostConstruct
    private void init(){
        this.setPosDevice(executionHelper.isTest() ? new SimulatedTestingMSR() : new MSR());
    }

//    protected MsrServiceManager() {
//        this.setDeviceType("Msr");
//        this.setPosDevice(new MSR());
//    }
//
//    protected MsrServiceManager(String logicalName){
//        this.setDeviceType("Msr");
//        this.setPosDevice(new MSR());
//        this.setDeviceLogicalName(logicalName);
//    }

    //</editor-fold>

    //<editor-fold desc="Methods">

    private void setDeviceProperties() throws JposException{
        Assert.notNull(this.getPosDevice(), MessageFormat.format("MsrServiceManager->setDeviceProperties: {0} {1} instance not set.",this.getDeviceLogicalName(),this.getDeviceType()));

        if (!((MSR) this.getPosDevice()).getDataEventEnabled()) {
            ((MSR)this.getPosDevice()).setDataEventEnabled(true);
            ((MSR)this.getPosDevice()).setDecodeData(true);
            ((MSR)this.getPosDevice()).setAutoDisable(false);
            ((MSR)this.getPosDevice()).setFreezeEvents(false);
        }

        bindListeners();
    }

    private void bindListeners(){
        Assert.notNull(this.getPosDevice(), MessageFormat.format("MsrServiceManager->setDeviceProperties: {0} {1} instance not set.",this.getDeviceLogicalName(),this.getDeviceType()));

        ((MSR)this.getPosDevice()).addDataListener(this);
        ((MSR)this.getPosDevice()).addStatusUpdateListener(this);
        ((MSR)this.getPosDevice()).addErrorListener(this);
    }

    private void unbindListeners(){
        Assert.notNull(this.getPosDevice(), MessageFormat.format("MsrServiceManager->setDeviceProperties: {0} {1} instance not set.",this.getDeviceLogicalName(),this.getDeviceType()));

        ((MSR)this.getPosDevice()).removeDataListener(this);
        ((MSR)this.getPosDevice()).removeStatusUpdateListener(this);
        ((MSR)this.getPosDevice()).removeErrorListener(this);
    }

    private void clearMsrData() throws JposException{
        if(this.getPosDevice() == null){
            return;
        }

        ((MSR)this.getPosDevice()).clearInput();
        ((MSR)this.getPosDevice()).setDataEventEnabled(true);

        log.info(MessageFormat.format("MsrServiceManager->clearMsrData: data cleared on MSR device {0} {1}",this.getDeviceLogicalName(),this.getDeviceType()));
    }

    //</editor-fold>

    //<editor-fold desc="Overridden Methods">

    @Override
    public void enableDevice() throws JposException {
        super.enableDevice();
        this.setDeviceProperties();
    }

    @Override
    public void disableDevice() throws JposException {
        this.unbindListeners();
        super.disableDevice();
    }

    @Override
    public void sendData(Hardware hardwareObject, Hardware resultObject) throws Exception {
        throw new NotImplementedException("Msr does not implement sendData method");
    }

    //</editor-fold>

    //<editor-fold desc="Listeners Method Overrides">

    @Override
    public void dataOccurred(DataEvent e) {
        try{
            log.info(MessageFormat.format("MsrServiceManager->dataOccurred: device with name {0} of type {1} received data.",this.getDeviceLogicalName(),this.getDeviceType()));

            Msr msrDto = new Msr(HardwareActionType.ReceivedData);
            msrDto.setAccountNumber(((MSR)this.getPosDevice()).getAccountNumber());
            msrDto.setExpirationDate(((MSR)this.getPosDevice()).getExpirationDate());
            msrDto.setFirstName(((MSR)this.getPosDevice()).getFirstName());
            msrDto.setMiddleName(((MSR)this.getPosDevice()).getMiddleInitial());
            msrDto.setSurname(((MSR)this.getPosDevice()).getSurname());
            msrDto.setTitle(((MSR)this.getPosDevice()).getTitle());
            msrDto.setServiceCode(((MSR)this.getPosDevice()).getServiceCode());
            msrDto.setSuffix(((MSR)this.getPosDevice()).getSuffix());
            msrDto.setTrack1Data(((MSR)this.getPosDevice()).getTrack1Data());
            msrDto.setTrack2Data(((MSR)this.getPosDevice()).getTrack2Data());

            msrDto.setActionResult(true);
            msrDto.setObjectStatus(this.getDeviceStatus());

            service.sendDeviceData(msrDto);

            this.clearMsrData();

        }catch (Exception ex){
            log.error("Exception on MsrServiceManager->dataOccurred",ex);
        }
    }

    @Override
    public void statusUpdateOccurred(StatusUpdateEvent e){
        if(this.getPosDevice() == null){
            return;
        }

        try {
            Msr msrDto = new Msr(HardwareActionType.StatusUpdated);
            msrDto.setAccountNumber(((MSR) this.getPosDevice()).getAccountNumber());
            msrDto.setExpirationDate(((MSR) this.getPosDevice()).getExpirationDate());
            msrDto.setFirstName(((MSR) this.getPosDevice()).getFirstName());
            msrDto.setMiddleName(((MSR) this.getPosDevice()).getMiddleInitial());
            msrDto.setSurname(((MSR) this.getPosDevice()).getSurname());
            msrDto.setTitle(((MSR) this.getPosDevice()).getTitle());
            msrDto.setServiceCode(((MSR) this.getPosDevice()).getServiceCode());
            msrDto.setSuffix(((MSR) this.getPosDevice()).getSuffix());
            msrDto.setTrack1Data(((MSR) this.getPosDevice()).getTrack1Data());
            msrDto.setTrack2Data(((MSR) this.getPosDevice()).getTrack2Data());

            msrDto.setObjectStatus(this.getDeviceStatus());

            //todo: TCP send data routine HERE!

            log.info(MessageFormat.format("MsrServiceManager->statusUpdateOccurred: Status {0}",e.getStatus()));
        }catch (Exception ex){
            log.error("Exception on MsrServiceManager->statusUpdateOccurred",ex);
        }
    }

    @Override
    public void errorOccurred(ErrorEvent e) {
        log.error("Error occurred on MSR device instance",e);
    }

    //</editor-fold>

    public void triggerMsrScan(Msr msr) throws Exception{
        if(this.getPosDevice() instanceof SimulatedTestingMSR){
            ((SimulatedTestingMSR) this.getPosDevice()).triggerDataEvent(msr.getTrack1Data().toString(), msr.getTrack2Data().toString(),"", "", msr.getAccountNumber(), msr.getExpirationDate(),
                    msr.getTitle(), msr.getFirstName(), msr.getMiddleName(), msr.getSurname(), msr.getSuffix(), msr.getServiceCode(), "", "");
        }else{
            throw new Exception("triggerMsrScan method is not available outside of testing execution context");
        }
    }
}
