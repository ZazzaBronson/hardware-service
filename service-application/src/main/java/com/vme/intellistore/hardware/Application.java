package com.vme.intellistore.hardware;

import com.vme.intellistore.hardware.helpers.ExecutionHelper;
import com.vme.intellistore.javapos.device.simulator.util.ExecutionState;
import org.springframework.boot.ExitCodeGenerator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Created by Mirko on 12-Jan-17.
 */
@SpringBootApplication
public class Application {

    private static ConfigurableApplicationContext context;

    public static void main(String[] args){
        boolean setHeadless = false;
        boolean enableWeb = false;
//        SpringApplication.run(Application.class,args);
        if(args.length > 0){
            String hValue = args[0];
            setHeadless = Boolean.parseBoolean(hValue);

            String wValue = args[1];
            enableWeb = Boolean.parseBoolean(wValue);
        }

        context = new SpringApplicationBuilder(Application.class).headless(setHeadless).web(enableWeb).run(args);
        context.registerShutdownHook();
    }

    public static void stop(int returnCode){
        SpringApplication.exit(context,() -> returnCode);
    }
}
