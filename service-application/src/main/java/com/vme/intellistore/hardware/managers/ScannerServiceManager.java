package com.vme.intellistore.hardware.managers;

import com.vme.intellistore.hardware.commons.dtos.devices.Hardware;
import com.vme.intellistore.hardware.commons.dtos.devices.HardwareActionType;
import com.vme.intellistore.hardware.helpers.ExecutionHelper;
import com.vme.intellistore.javapos.device.simulator.testing.beans.SimulatedTestingScanner;
import jpos.JposException;
import jpos.Scanner;
import jpos.events.DataEvent;
import jpos.events.DataListener;
import jpos.events.ErrorEvent;
import jpos.events.ErrorListener;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.NotImplementedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedOperationParameter;
import org.springframework.jmx.export.annotation.ManagedResource;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.annotation.PostConstruct;
import javax.management.ObjectName;
import java.nio.charset.Charset;
import java.text.MessageFormat;

/**
 * Created by Mirko on 18-Jan-17.
 */

@Component
@Slf4j
@Scope("prototype")
public class ScannerServiceManager extends HardwareServiceManager implements DataListener, ErrorListener{

    @Autowired
    private ExecutionHelper executionHelper;

    //<editor-fold desc="Constructors">

    protected ScannerServiceManager() {
        this.setDeviceType("Scanner");
    }

    @PostConstruct
    private void init(){
        this.setPosDevice(executionHelper.isTest() ? new SimulatedTestingScanner() : new Scanner());
    }

    //</editor-fold>

    //<editor-fold desc="Overridden Methods">

    @Override
    public void enableDevice() throws JposException {
        super.enableDevice();
        this.setDeviceProperties();
    }

    @Override
    public void disableDevice() throws JposException {
        this.unbindListeners();
        super.disableDevice();
    }

    @Override
    public void sendData(Hardware hardwareObject, Hardware resultObject) throws Exception {
        throw new NotImplementedException("Scanner does not implement sendData method");
    }

    //</editor-fold>

    //<editor-fold desc="Methods">

    private void setDeviceProperties() throws JposException{
        Assert.notNull(this.getPosDevice(), MessageFormat.format("{0} {1} instance not set.",this.getDeviceLogicalName(),this.getDeviceType()));

        if(!((Scanner)this.getPosDevice()).getDataEventEnabled()) {
            ((Scanner) this.getPosDevice()).setDataEventEnabled(true);
        }

        ((Scanner)this.getPosDevice()).setDecodeData(true);
        this.bindListeners();
    }

    private void bindListeners(){
        Assert.notNull(this.getPosDevice(), MessageFormat.format("{0} {1} instance not set.",this.getDeviceLogicalName(),this.getDeviceType()));

        ((Scanner)this.getPosDevice()).addDataListener(this);
        ((Scanner)this.getPosDevice()).addErrorListener(this);
    }

    private void unbindListeners(){
        Assert.notNull(this.getPosDevice(), MessageFormat.format("{0} {1} instance not set.",this.getDeviceLogicalName(),this.getDeviceType()));

        ((Scanner)this.getPosDevice()).removeDataListener(this);
        ((Scanner)this.getPosDevice()).removeErrorListener(this);
    }

    private void clearScanData() throws JposException{
        Assert.notNull(this.getPosDevice(),MessageFormat.format("{0} {1} instance not set.",this.getDeviceLogicalName(),this.getDeviceType()));

        ((Scanner)this.getPosDevice()).clearInput();
        ((Scanner)this.getPosDevice()).setDataEventEnabled(true);

        log.info(MessageFormat.format("Clearing scanner data on device {0} of type {1}",this.getDeviceLogicalName(),this.getDeviceType()));
    }

    //</editor-fold>

    //<editor-fold desc="Listener Method Overrides">

    @Override
    public void dataOccurred(DataEvent e) {
        try{
            byte[] data = ((Scanner)this.getPosDevice()).getScanData();
            log.info(MessageFormat.format("Device {0} of type {1} received data: {2}",this.getDeviceLogicalName(),this.getDeviceType(),new String(data, Charset.forName("UTF-8"))));

            com.vme.intellistore.hardware.commons.dtos.devices.Scanner scannerObject = new com.vme.intellistore.hardware.commons.dtos.devices.Scanner(HardwareActionType.ReceivedData);
            scannerObject.setData(data);
            scannerObject.setDataString(new String(data, Charset.forName("UTF-8")));
            scannerObject.setActionResult(true);
            scannerObject.setObjectStatus(this.getDeviceStatus());
            service.sendDeviceData(scannerObject);

            this.clearScanData();
        }catch (Exception ex){
            log.error("Exception occurred during dataOccurred invocation",ex);
        }
    }

    @Override
    public void errorOccurred(ErrorEvent e) {
        log.error("Error occurred on Scanner device instance",e);
    }

    //</editor-fold>

    public void triggerBarcodeScan(String barcode) throws Exception{
        if(this.getPosDevice() instanceof SimulatedTestingScanner){
            try {
                ((SimulatedTestingScanner) this.getPosDevice()).triggerScanEvent(barcode);
            }catch (Exception e){
                log.error("Exception occurred whilst executing triggerBarcodeScan method through MBean",e);
            }
        }else{
            throw new Exception("triggerBarcodeScan method is not available outside of testing execution context");
        }
    }
}
