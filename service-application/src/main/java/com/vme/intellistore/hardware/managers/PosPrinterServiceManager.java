package com.vme.intellistore.hardware.managers;

import com.vme.intellistore.hardware.commons.dtos.devices.BarcodeSymbology;
import com.vme.intellistore.hardware.commons.dtos.devices.Hardware;
import com.vme.intellistore.hardware.commons.dtos.devices.PosPrinter;
import com.vme.intellistore.hardware.commons.dtos.devices.printer.*;
import com.vme.intellistore.hardware.helpers.TextHelper;
import com.vme.intellistore.hardware.utils.DeviceUtils;
import com.vme.intellistore.javapos.device.simulator.testing.beans.SimulatedTestingPOSPrinter;
import jpos.JposConst;
import jpos.JposException;
import jpos.POSPrinter;
import jpos.events.ErrorEvent;
import jpos.events.ErrorListener;
import jpos.events.StatusUpdateEvent;
import jpos.events.StatusUpdateListener;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.annotation.PostConstruct;
import java.text.MessageFormat;
import java.util.LinkedHashMap;
import java.util.List;

import static jpos.POSPrinterConst.PTR_BC_CENTER;
import static jpos.POSPrinterConst.PTR_BM_CENTER;
import static jpos.POSPrinterConst.PTR_S_RECEIPT;

/**
 * Created by Mirko on 26-Jan-17.
 */
@Component
@Slf4j
@Scope("prototype")
public class PosPrinterServiceManager extends HardwareServiceManager implements StatusUpdateListener, ErrorListener{

    //<editor-fold desc="Constants">

    private static final int ReceiptLineCharacterLength = 40;

    //</editor-fold>

    //<editor-fold desc="Properties">

    private PosPrinter posPrinterObject;

    //</editor-fold>

    //<editor-fold desc="Constructors">

    protected PosPrinterServiceManager(){
        this.setDeviceType("Pos Printer");
    }

    @PostConstruct
    private void init(){
        this.setPosDevice(executionHelper.isTest() ? new SimulatedTestingPOSPrinter() : new POSPrinter());
    }

//    protected PosPrinterServiceManager() {
//        this.setDeviceType("Pos Printer");
//        this.setPosDevice(new POSPrinter());
//    }
//
//    protected PosPrinterServiceManager(String logicalName){
//        this.setDeviceType("Pos Printer");
//        this.setPosDevice(new POSPrinter());
//        this.setDeviceLogicalName(logicalName);
//    }

    //</editor-fold>

    //<editor-fold desc="Methods">

    private void setDeviceProperties(){
        Assert.notNull(this.getPosDevice(), MessageFormat.format("{0} {1} instance not set.",this.getDeviceLogicalName(),this.getDeviceType()));

        try{
            ((POSPrinter)this.getPosDevice()).setRecLineChars(44);
            ((POSPrinter)this.getPosDevice()).setRecLineSpacing(24);

            for (int supportedCharacterSet : DeviceUtils.getCharacterSets(((POSPrinter)this.getPosDevice()).getCharacterSetList())) {
                if(this.posPrinterObject.getCharacterSet() != supportedCharacterSet){
                    continue;
                }

                ((POSPrinter)this.getPosDevice()).setCharacterSet(this.posPrinterObject.getCharacterSet());
                break;
            }

            this.bindListeners();
        }catch (Exception e){
            log.error("Error occurred in PosPrinterDeviceService->setDeviceProperties",e);
        }
    }

    private void bindListeners(){
        Assert.notNull(this.getPosDevice(), MessageFormat.format("{0} {1} instance not set.",this.getDeviceLogicalName(),this.getDeviceType()));

        ((POSPrinter)this.getPosDevice()).addStatusUpdateListener(this);
        ((POSPrinter)this.getPosDevice()).addErrorListener(this);
    }

    private void unbindListeners(){
        Assert.notNull(this.getPosDevice(), MessageFormat.format("{0} {1} instance not set.",this.getDeviceLogicalName(),this.getDeviceType()));

        ((POSPrinter)this.getPosDevice()).removeStatusUpdateListener(this);
        ((POSPrinter)this.getPosDevice()).removeErrorListener(this);
    }

    private String centerText(String inputText, int length, boolean trim){
        if(length <= 0){
            return "";
        }

        if(inputText.isEmpty()){
            return TextHelper.padLeft("",length);
        }

        String output = inputText;

        if(trim){
            output = output.trim();
        }

        int whiteSpaceCount = length - output.length();

        if(whiteSpaceCount < 0){
            return output.substring(0,length);
        }

        int leftPadding = whiteSpaceCount / 2;

        return TextHelper.padRight(TextHelper.padLeft(output,leftPadding+output.length()),output.length());
    }

    public void printLines(List<Object> printerLines) throws Exception{
        synchronized (this){
            if(printerLines == null || printerLines.size() == 0){
                return;
            }

            for(Object item : printerLines){
                LinkedHashMap line = (LinkedHashMap) item;
                PrinterLineType type = PrinterLineType.valueOf((String)line.get("LineType"));

                if(type.equals(PrinterLineType.TextLine)){
                    this.printText(PrinterTextLine.convert((LinkedHashMap)item).getTextLines());
                }else if (type.equals(PrinterLineType.BarcodeLine)){
                    this.printBarcode(PrinterBarcodeLine.convert((LinkedHashMap)item));
                }else if(type.equals(PrinterLineType.ImageLine)){
                    PrinterImageLine imageLine = (PrinterImageLine) item;
                    this.printImage(imageLine.getImageLocation(),imageLine.isAddNewLineAfter(),imageLine.getWidth(),PTR_BM_CENTER);
                }else if (type.equals(PrinterLineType.CutPaperLine)){
                    PrinterCutPaperLine cutLine = PrinterCutPaperLine.convert(line);
                    this.cutPaper(cutLine.getCutPaperPercentage(),cutLine.getNoLinesToSkip());
                }
            }
        }
    }

    public void printBarcode(PrinterBarcodeLine line) throws JposException{
        synchronized (this){
            if(this.getPosDevice() != null &&
                    ((POSPrinter)this.getPosDevice()).getState() == JposConst.JPOS_S_IDLE &&
                    ((POSPrinter)this.getPosDevice()).getClaimed() == true &&
                    ((POSPrinter)this.getPosDevice()).getDeviceEnabled() == true){
                try{
                    this.sendTextToPrinter("\n");
                    ((POSPrinter)this.getPosDevice()).printBarCode(PTR_S_RECEIPT,
                            line.getBarcode(),
                            line.getType().getNumVal(),
                            line.getHeight(),
                            line.getWidth(),
                            line.getPosition(),
                            line.getTextPosition().getNumVal());
                }catch(Exception e){
                    log.error("An exception has occurred PosPrinteDeviceService->printBarcode",e);
                }
            }
        }
    }

    public void printImage(String fileName, Boolean addNewLineAfter, int width, int alignment) throws JposException{
        synchronized (this){
            if(this.getPosDevice() != null &&
                    ((POSPrinter)this.getPosDevice()).getState() == JposConst.JPOS_S_IDLE &&
                    this.getPosDevice().getClaimed() == true &&
                    this.getPosDevice().getDeviceEnabled() == true){
                try{
                    log.info(MessageFormat.format("Printing image {0}",fileName));

                    ((POSPrinter)this.getPosDevice()).printBitmap(PTR_S_RECEIPT,fileName,width,alignment);

                    if(addNewLineAfter){
                        this.sendTextToPrinter("\n");
                    }
                }catch (Exception e){
                    log.error("An exception occurred PosPrinterServiceManager->printImage",e);
                }
            }
        }
    }

    public void printText(List<String> textLines) throws Exception{
        synchronized (this){
            Assert.notNull(this.getPosDevice(),MessageFormat.format("{0} {1} instance not set.",this.getDeviceLogicalName(),this.getDeviceType()));
            Assert.isTrue(this.canProcess(),"PosPrinter is not initialised");

            if(textLines == null || textLines.size() == 0){
                throw new Exception("No receipt lines to print");
            }

            StringBuilder receiptBuilder = new StringBuilder();

            ((POSPrinter)this.getPosDevice()).setRecLineChars(24);

            int width = ((POSPrinter)this.getPosDevice()).getRecLineChars() != 0 ? ((POSPrinter)this.getPosDevice()).getRecLineChars() : ReceiptLineCharacterLength;

            for (String line : textLines) {
                String textLine = this.centerText(line,width,false);
                receiptBuilder.append(textLine + "\n");
            }

            this.sendTextToPrinter(receiptBuilder.toString());
        }
    }

    public void sendTextToPrinter(String text) throws JposException{
        log.info(MessageFormat.format("Received text to print: {0}",text));

        if(this.posPrinterObject.getCurrencySymbol() != null)
        text = text.replace((char)339,this.posPrinterObject.getCurrencySymbol().toCharArray()[0]);

        if(this.posPrinterObject.getCurrencySymbolASCII() != null)
        text = text.replace(this.posPrinterObject.getCurrencySymbol().toCharArray()[0],this.posPrinterObject.getCurrencySymbolASCII().toCharArray()[0]);

        ((POSPrinter)this.getPosDevice()).printNormal(PTR_S_RECEIPT,text);
    }

    public void cutPaper(int percentageToCut, int noLinesToPrintBeforeCut) throws JposException{
        synchronized (this){
            log.info("Cutting paper");

            if(noLinesToPrintBeforeCut == 0){
                noLinesToPrintBeforeCut = ((POSPrinter)this.getPosDevice()).getRecLinesToPaperCut();
            }

            String newLines = "";

            for(int i = 0; i < noLinesToPrintBeforeCut; i++){
                newLines += "\n";
            }

            this.sendTextToPrinter(newLines);
            ((POSPrinter)this.getPosDevice()).cutPaper(percentageToCut);
        }
    }

    //</editor-fold>

    //<editor-fold desc="Overridden Methods">

    @Override
    public void initialise(Hardware hardwareObject) throws JposException {
        this.posPrinterObject = (PosPrinter) hardwareObject;
        super.initialise(hardwareObject);
    }

    @Override
    public void enableDevice() throws JposException {
        super.enableDevice();
        this.setDeviceProperties();
    }

    @Override
    public void disableDevice() throws JposException {
        this.unbindListeners();
        super.disableDevice();
    }

    @Override
    public void sendData(Hardware hardwareObject, Hardware resultObject) throws Exception {
        Assert.notNull(this.getPosDevice(), MessageFormat.format("{0} {1} instance not set.",this.getDeviceLogicalName(),this.getDeviceType()));
        Assert.notNull(hardwareObject, "No Hardware object dto provided on PosPrinterServiceManager->sendData");
        Assert.isInstanceOf(PosPrinter.class,hardwareObject,MessageFormat.format("Hardware Object dto provided is of type {0}, type {1} expected instead",hardwareObject.getClass(),PosPrinter.class));

        PosPrinter requestObject = (PosPrinter) hardwareObject;

        if(requestObject.getPrinterLines() != null && requestObject.getPrinterLines().getValues().size() > 0){
            this.printLines(requestObject.getPrinterLines().getValues());
        }else if(requestObject.getTextLinesToPrint() != null){
            this.printText(requestObject.getTextLinesToPrint());
        }else if(requestObject.getBarcode() != null && !requestObject.getBarcode().isEmpty()){
            this.printBarcode(new PrinterBarcodeLine(requestObject.getBarcode(),requestObject.getBarcodeHeight(),requestObject.getBarcodeWidth(),requestObject.getBarcodePosition(),requestObject.getBarcodeType(),requestObject.getTextPosition()));
        }else if(requestObject.getImageLocation() != null && !requestObject.getImageLocation().isEmpty()){
            this.printImage(requestObject.getImageLocation(),requestObject.isAddNewLineAfterImagePrint(),requestObject.getImageWidth(),PTR_BC_CENTER);
        }else if(requestObject.getCutPaperPercentage() > 0){
            this.cutPaper(requestObject.getCutPaperPercentage(),requestObject.getNoLinesToSkip());
        }
    }

    //</editor-fold>

    //<editor-fold desc="Listeners Overridden Methods">

    @Override
    public void statusUpdateOccurred(StatusUpdateEvent e) {
        Assert.notNull(this.getPosDevice(), MessageFormat.format("{0} {1} instance not set.",this.getDeviceLogicalName(),this.getDeviceType()));
        Assert.isTrue(this.canProcess(),MessageFormat.format("{0} {1} is not initialised",this.getDeviceLogicalName(), this.getDeviceType()));

        log.info(MessageFormat.format("PosPrinterServiceManager->statusUpdateOccurred: Status {0}",e.getStatus()));

        switch (e.getStatus()){
            case 10010:
                log.info("PosPrinterServiceManager->statusUpdateOccurred: WARNING - cutter jam detected");
                break;
            case 10011:
                log.info("PosPrinterServiceManager->statusUpdateOccurred: WARNING - only a partial cut was performed");
                break;
            default:
                log.info(MessageFormat.format("PosPrinterServiceManager->statusUpdateOccurred: Unknown status {0}",e.getStatus()));
                break;
        }
    }

    @Override
    public void errorOccurred(ErrorEvent e) {
        log.error("An error occurred on PosPrinter device instance",e);
    }

    //</editor-fold>

    public String getTextData() throws Exception{
        if(this.getPosDevice() instanceof SimulatedTestingPOSPrinter){
            return ((SimulatedTestingPOSPrinter) this.getPosDevice()).getTextData();
        }else{
            throw new Exception("getPrintedData method cannot be invoked outside of Testing execution context");
        }
    }
}
