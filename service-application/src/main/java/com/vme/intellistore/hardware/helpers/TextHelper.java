package com.vme.intellistore.hardware.helpers;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.NotImplementedException;

import java.text.MessageFormat;

/**
 * The TextHelper class is host to all text-centric functions, required by the application
 */
@Slf4j
public class TextHelper {

    //<editor-fold desc="Constants">

    private static final String EMPTY = "";

    private static final int ArabicOemCodePage = 720;
    private static final int ArabicSbcsCodePage = 1256;
    private static final int ASCII = 998;
    private static final int BalticOemCodePage = 775;
    private static final int BalticSbcsCodePage = 1257;
    private static final int CentralEuropeCodePage = 1250;
    private static final int CyrillicCodePage = 1251;
    private static final int GreekCodePage = 1253;
    private static final int HebrewOemCodePage = 862;
    private static final int HebrewSbcsCodePage = 1255;
    private static final int LatinICodePage = 1252;
    private static final int MsDosArabicCodePage = 864;
    private static final int MsDosFrenchCanadaCodePage = 863;
    private static final int MsDosGreekCodePage = 869;
    private static final int MultiLingualLatinAndEuroCodePage = 858;
    private static final int MultiLingualLatinCodePage = 850;
    private static final int ThaiCodePage = 874;
    private static final int TurkishOemCodePage = 857;
    private static final int TurkishSbcsCodePage = 1254;
    private static final int UsCodePage = 437;
    private static final int VietnamCodePage = 1258;
    private static final int Windows31J = 932;

    //</editor-fold>

    public static String center(String inputText, int outputLength, boolean trimInputText){
        if(outputLength <= 0){
            return inputText;
        }

        if(inputText == null || inputText.isEmpty()){
            return padLeft(EMPTY,outputLength);
        }

        String trimmedInput = trimInputText ? inputText.trim() : inputText;

        int whitespaceCount = outputLength - trimmedInput.length();

        if(whitespaceCount < 0){
            return trimmedInput.substring(0,outputLength);
        }

        int padCountLeft = whitespaceCount / 2;
//        int padCountRight = outputLength - trimmedInput.length() - padCountLeft;

        return padRight(padLeft(trimmedInput,padCountLeft +  trimmedInput.length()),outputLength);
    }

    public static String padLeft(String s, int n){
        return String.format("%1$" + n + "s",s);
    }

    public static String padRight(String s, int n){
        return String.format("%1$-" + n + "s",s);
    }

    public static String formatForLineDisplay(String text){
        String lineDisplayOutput = text;

        if(lineDisplayOutput.length() > 20){
            lineDisplayOutput = lineDisplayOutput.substring(0,20);
        }

        return lineDisplayOutput;
    }

    public static int getCodePageValueForSymbol(int codePage, String symbol, String symbolOverride,int symbolASCII){
        int returnObject;

        if(!symbolOverride.isEmpty() && symbol.equals("£")){
            returnObject = Integer.parseInt(symbolOverride);
            log.info(MessageFormat.format("Override for LineDisplay currency symbol set up, setting symbol to {0}",returnObject));
        }else{
            switch (symbol){
                case "£":
                    switch (codePage){
                        case MsDosFrenchCanadaCodePage:
                        case MsDosGreekCodePage:
                        case UsCodePage:
                        case ASCII:
                            returnObject = 339;
                            break;
                        case LatinICodePage:
                        case GreekCodePage:
                        case TurkishSbcsCodePage:
                        case HebrewSbcsCodePage:
                        case ArabicSbcsCodePage:
                        case BalticSbcsCodePage:
                        case VietnamCodePage:
                        case ArabicOemCodePage:
                        case BalticOemCodePage:
                        case MultiLingualLatinCodePage:
                        case TurkishOemCodePage:
                        case MultiLingualLatinAndEuroCodePage:
                        case HebrewOemCodePage:
                        case MsDosArabicCodePage:
                            returnObject = 163;
                            break;
                        case Windows31J:
                            returnObject = 35;
                            break;
                        case 852:
                        case 855:
                        case 865:
                        case 866:
                        case 999:
                        default:
                            returnObject = symbolASCII;
                            break;
                    }
                    break;
                case "€":
                    switch (codePage){
                        case MsDosFrenchCanadaCodePage:
                        case MsDosGreekCodePage:
                        case UsCodePage:
                        case ASCII:
                            returnObject = 128; // Ascii
                            break;
                        case LatinICodePage:
                        case GreekCodePage:
                        case TurkishSbcsCodePage:
                        case HebrewSbcsCodePage:
                        case ArabicSbcsCodePage:
                        case BalticSbcsCodePage:
                        case VietnamCodePage:
                        case ArabicOemCodePage:
                        case BalticOemCodePage:
                        case MultiLingualLatinCodePage:
                        case TurkishOemCodePage:
                        case MultiLingualLatinAndEuroCodePage:
                        case HebrewOemCodePage:
                        case MsDosArabicCodePage:
                            returnObject = 8364; // Unicode
                            break;
                        case 852:
                        case 855:
                        case 865:
                        case 866:
                        case 932:
                        case 999:
                        default:
                            returnObject = '€';
                            break;
                    }
                    break;
                default:
                    log.error("No codePage conversion support for LineDisplay symbol: "+symbol);
                    throw new NotImplementedException();
            }
        }

        return returnObject;
    }
}
