package com.vme.intellistore.hardware.services;

import com.vme.intellistore.hardware.commons.dtos.devices.HardwareDeviceType;
import com.vme.intellistore.hardware.commons.exceptions.DeviceNotImplementedException;
import com.vme.intellistore.hardware.configuration.BaseConfiguration;
import com.vme.intellistore.hardware.configuration.DeviceConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.net.URI;
import java.util.Properties;

/**
 * Created by Zazza on 09/05/2017.
 */
@Service
@Slf4j
public class ConfigurationService {
    @Autowired
    private BaseConfiguration baseConfiguration;

    @Autowired
    private DeviceConfiguration deviceConfiguration;

    private Properties loadProperties(){
        ClassLoader loader = getClass().getClassLoader();
        File configFile = new File(loader.getResource("application.properties").getFile());

        if(!configFile.exists()){
            configFile = new File(this.getRootPath()+"application.properties");
        }

        Properties props = new Properties();

        try{
            FileReader reader = new FileReader(configFile);
            props.load(reader);

            reader.close();

        }catch (Exception e){
            log.error("Error occurred",e);
        }

        return props;
    }

    private void saveProperties(Properties props){
        ClassLoader loader = getClass().getClassLoader();
        File configFile = new File(loader.getResource("application.properties").getFile());

        if(!configFile.exists()){
            configFile = new File(this.getRootPath()+"application.properties");
        }

        try{
            FileWriter writer = new FileWriter(configFile);
            props.store(writer,"Updated entry");
        }catch (Exception e){
            log.error("Error occurred",e);
        }
    }

    private void updateProperty(String key, String value){
        Properties properties = this.loadProperties();

        properties.setProperty(key,value);
        this.saveProperties(properties);
    }

    public void setSelectedDevice(HardwareDeviceType type, String name) throws DeviceNotImplementedException{
        switch (type){
            case CashDrawer:
                deviceConfiguration.setNameCashDrawer(name);
                this.updateProperty("devices.nameCashDrawer",name);
                break;
            case LineDisplay:
                deviceConfiguration.setNameLineDisplay(name);
                this.updateProperty("devices.nameLineDisplay",name);
                break;
            case MSR:
                deviceConfiguration.setNameMsr(name);
                this.updateProperty("devices.nameMsr",name);
                break;
            case POSPrinter:
                deviceConfiguration.setNamePosPrinter(name);
                this.updateProperty("devices.namePosPrinter",name);
                break;
            case Scale:
                deviceConfiguration.setNameScale(name);
                this.updateProperty("devices.nameScale",name);
                break;
            case Scanner:
                deviceConfiguration.setNameScanner(name);
                this.updateProperty("devices.nameScanner",name);
                break;
            default:
                throw new DeviceNotImplementedException(String.format("Device of type {0} is not implemented yet",type.toString()));
        }
    }

    private String getRootPath(){
        return new File(System.getProperty("user.dir")).toURI().getPath();
    }
}
