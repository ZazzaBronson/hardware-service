package com.vme.intellistore.hardware.managers;

import com.vme.intellistore.hardware.commons.TextAlignment;
import com.vme.intellistore.hardware.commons.dtos.devices.Hardware;
import com.vme.intellistore.hardware.commons.exceptions.DeviceNotInitialisedException;
import com.vme.intellistore.hardware.utils.DeviceUtils;
import com.vme.intellistore.hardware.helpers.TextHelper;
import com.vme.intellistore.javapos.device.simulator.testing.beans.SimulatedTestingLineDisplay;
import jpos.JposException;
import jpos.LineDisplay;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.annotation.PostConstruct;
import java.text.MessageFormat;
import java.util.ArrayList;

import static jpos.LineDisplayConst.DISP_DT_NORMAL;

/**
 * Created by Mirko on 24-Jan-17.
 */
@Component
@Slf4j
@Scope("prototype")
public class LineDisplayServiceManager extends HardwareServiceManager {

    private com.vme.intellistore.hardware.commons.dtos.devices.LineDisplay lineDisplayObject;

    //<editor-fold desc="Constructors">

    protected LineDisplayServiceManager() {
        this.setDeviceType("Line Display");
    }

    @PostConstruct
    private void init(){
        this.setPosDevice(executionHelper.isTest() ? new SimulatedTestingLineDisplay() : new LineDisplay());
    }

//    protected LineDisplayServiceManager() {
//        this.setDeviceType("Line Display");
//        this.setPosDevice(new LineDisplay());
//    }
//
//    protected LineDisplayServiceManager(String logicalName){
//        this.setDeviceType("Line Display");
//        this.setPosDevice(new LineDisplay());
//        this.setDeviceLogicalName(logicalName);
//    }

    //</editor-fold>

    //<editor-fold desc="Overridden Methods">

    @Override
    public void enableDevice() throws JposException {
        super.enableDevice();
        this.setDeviceProperties();
    }

    @Override
    public void initialise(Hardware hardwareObject) throws JposException {
        this.lineDisplayObject = new com.vme.intellistore.hardware.commons.dtos.devices.LineDisplay(hardwareObject.getActionType());
        super.initialise(hardwareObject);
    }

    @Override
    public void sendData(Hardware hardwareObject, Hardware resultObject) throws Exception {
        Assert.notNull(this.getPosDevice(), MessageFormat.format("{0} {1} instance not set.",this.getDeviceLogicalName(),this.getDeviceType()));
        Assert.notNull(hardwareObject, "No Hardware object dto provided on LineDisplayServiceManager->sendData");
        //Assert.isInstanceOf(com.vme.intellistore.hardware.commons.dtos.devices.LineDisplay.class,hardwareObject,MessageFormat.format("Hardware Object dto provided is of type {0}, type {1} expected instead",hardwareObject.getClass(), com.vme.intellistore.hardware.commons.dtos.devices.LineDisplay.class));

        com.vme.intellistore.hardware.commons.dtos.devices.LineDisplay requestObject = (com.vme.intellistore.hardware.commons.dtos.devices.LineDisplay) hardwareObject;

        this.displayText(requestObject.getRowIndex(),requestObject.getTextToDisplay(),requestObject.getTextAlignment());
    }

    //</editor-fold>

    //<editor-fold desc="Methods">

    private ArrayList<Integer> getCharacterSets() throws JposException{
        String list = ((LineDisplay)this.getPosDevice()).getCharacterSetList();
        String[] values = list.split(",");

        ArrayList<Integer> characterSets = new ArrayList<Integer>();

        for (String item : values) {
            characterSets.add(Integer.parseInt(item));
        }

        return characterSets;
    }

    public void displayText(int rowIndex, String textToDisplay, TextAlignment alignment) throws JposException,DeviceNotInitialisedException{
        synchronized (this){
            Assert.notNull(this.getPosDevice(), MessageFormat.format("{0} {1} instance not set.",this.getDeviceLogicalName(),this.getDeviceType()));

            if(!this.canProcess()){
                throw new DeviceNotInitialisedException("LineDisplay device is not initialised");
            }

            log.info(MessageFormat.format("Received text to show on display: \"{0}\" or row {1}.",textToDisplay,rowIndex));

            textToDisplay = this.prepareText(textToDisplay,alignment);

            try{
                if(rowIndex <= (((LineDisplay)this.getPosDevice()).getDeviceRows() - 1)){
                    ((LineDisplay)this.getPosDevice()).displayTextAt(rowIndex,0,textToDisplay,DISP_DT_NORMAL);
                }else{
                    log.info(MessageFormat.format("Cannot display text [{0}] in row [{1}] as the line display only detects [{2}].",textToDisplay,rowIndex,((LineDisplay)this.getPosDevice()).getRows()));
                }
            }catch (Exception e){
                log.error("Exception occurred in LineDisplayServiceManager->displayText",e);
            }
        }
    }

    private String prepareText(String textToDisplay, TextAlignment alignment) throws JposException{
        int columns = ((LineDisplay)this.getPosDevice()).getColumns();

        switch (alignment){
            case Center:
                textToDisplay = TextHelper.center(textToDisplay,columns,true);
                break;
            case Right:
                textToDisplay = TextHelper.padRight(textToDisplay,columns);
                break;
            case Left:
                textToDisplay = TextHelper.padLeft(textToDisplay,columns);
                break;
            default:
                break;
        }

        textToDisplay = TextHelper.formatForLineDisplay(textToDisplay);

        if(this.lineDisplayObject != null && this.lineDisplayObject.getCurrencySymbol() != null){
            textToDisplay = textToDisplay.replace(this.lineDisplayObject.getCurrencySymbol().toCharArray()[0],
                    (char)TextHelper.getCodePageValueForSymbol(this.lineDisplayObject.getCharacterSet(),
                    this.lineDisplayObject.getCurrencySymbol(),
                    this.lineDisplayObject.getCurrencySymbolOverride(),
                    this.lineDisplayObject.getCurrencySymbolASCII()));

            textToDisplay = textToDisplay.replace('€', (char)TextHelper.getCodePageValueForSymbol(this.lineDisplayObject.getCharacterSet(),
                    "€",
                    this.lineDisplayObject.getCurrencySymbolOverride(),
                    this.lineDisplayObject.getCurrencySymbolASCII()));
        }

        return textToDisplay;
    }

    private void setDeviceProperties(){
        Assert.notNull(this.getPosDevice(),MessageFormat.format("{0} {1} instance not set.",this.getDeviceLogicalName(),this.getDeviceType()));

        try{

            for (int supportedSet : DeviceUtils.getCharacterSets(((LineDisplay)this.getPosDevice()).getCharacterSetList())) {
                if(this.lineDisplayObject.getCharacterSet() != supportedSet){
                    continue;
                }

                ((LineDisplay)this.getPosDevice()).setCharacterSet(this.lineDisplayObject.getCharacterSet());
            }

        }catch (Exception e){
            log.error("An exception has occurred whilst setting device propertied for LineDisplay",e);
        }
    }

    //</editor-fold>

    public String getLineDisplayData() throws Exception {
        if(this.getPosDevice() instanceof SimulatedTestingLineDisplay){
            return ((SimulatedTestingLineDisplay) this.getPosDevice()).getText();
        }else{
            throw new Exception("getLineDisplayData method cannot be accessed outside of the Testing execution context");
        }
    }
}
