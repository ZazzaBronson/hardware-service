package com.vme.intellistore.hardware.helpers;

import com.vme.intellistore.hardware.managers.ServiceManagerHelper;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Component
public class SpringContextBridge implements SpringContextBridgeServices, ApplicationContextAware {

    private static ApplicationContext applicationContext;

    @Autowired
    private ServiceManagerHelper serviceManagerHelper;

    @Override
    public ServiceManagerHelper getServiceManagerHelper() {
        return serviceManagerHelper;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    public static SpringContextBridgeServices services(){
        return applicationContext.getBean(SpringContextBridgeServices.class);
    }
}
